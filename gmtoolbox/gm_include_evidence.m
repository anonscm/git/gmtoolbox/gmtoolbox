function fg = gm_include_evidence( fg, evidence)
% Include evidence in a factor graph
% Arguments : 
%   fg : factor graph with nv variables (struct array)
%   evidence : observations of variables (vector 1 x nv) of integer
% Evaluation :
%   fg : new factor graph, possibly with more sub factor graphs (struct array)

if isfield(fg,'evidence')
    disp('ERROR: factor graph has yet evidence'); fg=[];
elseif length(evidence)~= length(fg.Card)
    disp('ERROR: length of evidence not correct'); fg=[];
elseif ~isnumeric(evidence) || any(ceil(evidence)~=evidence) || ...
        any(evidence<0) || any(evidence>fg.Card)
    disp('ERROR: at least one value of evidence not correct'); fg=[];
else
    fg.evidence=evidence;
    
    ve = find(evidence~=0);
    for isfg =1:length(fg.sfg)
        sfg = fg.sfg{isfg};
        vesfg_bool = ismember(sfg.Vcode, ve);
        if any(vesfg_bool) % at least one variable of the sfg is observed 
           oldVcode=sfg.Vcode;
           sfg.Vcode(vesfg_bool)=[]; % update Vcode without observed variables
           for f=1:length(sfg.F) % check all factors to update F
                V_bool=false(1,length(oldVcode)); V_bool(sfg.F{f}.V)=true;
                if any(V_bool & vesfg_bool)
                    V_remain_bool = and(V_bool,~vesfg_bool);
                    if any(V_remain_bool)
                        if isa(sfg.F{f}.T, 'function_handle'); T = sfg.F{f}.T();
                        else; T = sfg.F{f}.T; end
                        cmd='T=squeeze(T(';
                        for v=sfg.F{f}.V
                            if vesfg_bool(v); cmd=[cmd 'evidence(' int2str(v) '),'];
                            else; cmd=[cmd ':,'];end
                        end
                        cmd=cmd(1:(length(cmd)-1)); cmd=[cmd '));'];
                        eval(cmd);
                        if sum(size(T)-1)==1 && size(T,1)==1; T=T'; end
                        if  isa(sfg.F{f}.T, 'function_handle'); sfg.F{f}.T = @() T;
                        else; sfg.F{f}.T = T; end  
                        [~, Vnew] = ismember(find(V_remain_bool), sfg.Vcode); % update variables labels 
                        sfg.F{f}.V = Vnew;
                    else
                        sfg.F{f}=[];
                    end
                else
                    Vold = sfg.F{f}.V;
                    [~, Vnew] = ismember(Vold, sfg.Vcode); % variables labels in sfg_n
                    sfg.F{f}.V = Vnew;
                end
            end
            sfg.F = sfg.F( ~cellfun('isempty',sfg.F)); % remove emty factors (all observed)
            sfg.BG = setBG(length(sfg.Vcode),sfg.F);
            fg.sfg{isfg}=sfg;
        end
    end
    fg = gm_separate_fg(fg); % with evidence some sfg may be disconnected
end
end




