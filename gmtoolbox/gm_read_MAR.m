function b = gm_read_MAR( file_name )
% Read a .MAR UAI file containing marginals
% (format description: http://www.cs.huji.ac.il/project/PASCAL/fileFormat.php)
% BEWARE: ONLY the first belief sample is read !
% Arguments
%   file_name : name of the file to read
% Evaluation
%   b : beliefs of variables (cell array)

b={};
if exist(file_name, 'file') ~= 2
    disp('ERROR: not a such file found!');
else
    fid = fopen(file_name ,'rt');
    txt = fscanf(fid, '%c');
    txt = strsplit(txt,'\n');
    fclose(fid);
    
    b = {};
    if ~strcmp(txt{1},'MAR')
        disp('ERROR: not a MAR file !');
    elseif str2num(txt{2})~=1
        disp('WARNING: ONLY the first belief sample is read !');
    else
        MAR = strsplit(txt{3});
        nb_vars = str2num(MAR{1});
        
        if nb_vars <= 0
            disp('ERROR: invalid number of variables.');
        else
            cpt = 2;
            for i=1:nb_vars
                card = str2num(MAR{cpt}); cpt=cpt+1;
                b{i} = zeros(card,1);
                for j=1:card; b{i}(j) = str2num(MAR{cpt}); cpt=cpt+1; end
            end
        end
    end
end
end