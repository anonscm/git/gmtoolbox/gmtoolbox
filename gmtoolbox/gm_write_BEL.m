function gm_write_BEL( br , file_name)
% Write a .BEL UAI file containing beliefs of regions 
% (format description: http://www.cs.huji.ac.il/project/PASCAL/fileFormat.php)
% Arguments
%   br : beliefs of regions
%   file_name : name of the file to read


fid = fopen(file_name ,'wt');
fprintf(fid, 'BEL\n1\n');

nb_regions = 0; for i=1:length(br); nb_regions = nb_regions + length(br{i}); end
txt = int2str(nb_regions);

for i=1:length(br)
    bri=br{i};
    nb_regionsi = length(bri);
    for j=1:nb_regionsi
        T = bri{j};
        txt = [txt ' ' num2str(numel(T))];
        for k=1:numel(T)
            v_k=itov(k, size(T));
            cmd = ['T(' int2str(v_k(1))]; for z=2:length(v_k); cmd = [cmd ',' int2str(v_k(z))]; end; cmd = [cmd ')'];
            txt = [ txt ' ' num2str(eval(cmd), '%.4f')];
        end
    end
end
fprintf(fid, [txt '\n']);

fclose(fid);
end