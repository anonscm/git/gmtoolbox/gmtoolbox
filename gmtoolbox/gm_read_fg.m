function fg = gm_read_fg (file_name)
% Read an .uai UAI file containing a factor graph
% (format description: http://www.cs.huji.ac.il/project/PASCAL/fileFormat.php)
% Arguments
%   file_name : name of the file to read
% Evaluation
%   fg : a factor graph (structure array)

fg={};
if exist(file_name, 'file') ~= 2
    disp('ERROR: not a such file found!');
else
    fid = fopen(file_name ,'rt');
    txt = fscanf(fid, '%c');
    txt = strsplit(txt,'\n');
    fclose(fid);
    
    if ~strcmp(txt{1},'MARKOV') && ~strcmp(txt{1},'BAYES'); disp('ERROR: wrong type.'); end
    nb_vars = str2num(txt{2}); 
    if ~isnumeric(nb_vars); disp(['ERROR: the number of variables is not an integer']); end
    fg.Card = str2num(txt{3}); 
    if any(~isnumeric(fg.Card)); disp(['ERROR: cardinal of variables are not all integers']); end
    
    nb_factors = str2num(txt{4});
    sfg.Vcode = 1:nb_vars;
    
    
    F = cell(nb_factors,1);
    % Read variables of factors
    for f=1:nb_factors
        V = str2num(txt{4+f});
        nb_V = V(1); V(1) = '';
        if length(V)~=nb_V; disp(['ERROR: in factor ' int2str(f) ' bad number of variable.']); end
        F{f}.V=V+1; % Beware in Matlab, first variable is 1 (not 0)
    end
    % read tables of factors
    cpt = 4+nb_factors+1;
    for f=1:nb_factors
        t = str2num(txt{cpt}); cpt = cpt+1;
        while isempty(t); t = str2num(txt{cpt}); cpt = cpt+1; end  % sometimes an empty line between 2 tables
        nb_elt = t(1); t(1) = '';
        tab = [];
        while length(tab)<nb_elt
            while isempty(t); t = str2num(txt{cpt}); cpt = cpt+1; end % sometimes table is in the line after the nb of elements in the table
            tab = [tab t]; t=[];
        end
        if length(tab)~=nb_elt; disp(['ERROR: in factor ' int2str(f) ' bad number of element in table.']); end
        form =  fg.Card( F{f}.V ); if length(form)==1; T = zeros(form,1); else; T = zeros(form); end
        for i=1:nb_elt
            V_i = itov(i, fg.Card( F{f}.V ) );
            cmd = ['T(' int2str(V_i(1))]; for z=2:length(V_i); cmd = [cmd ',' int2str(V_i(z))]; end; cmd = [cmd ')=tab(i);'];
            eval(cmd);
        end
        F{f}.T = T;
    end
    sfg.F = F;
    fg.sfg{1} = sfg;
    
    
    % Compute sfg.BG and etect several sfg and compute fg.BG
    BG = false(nb_factors, nb_vars);
    for f=1:nb_factors; BG(f,F{f}.V)=true; end
    fg.sfg{1}.BG = BG;
    fg = gm_separate_fg(fg);
    if length(fg.sfg)~=1 ; disp(['WARNING: Several sub factor graphs were found.']); end
    
end
end