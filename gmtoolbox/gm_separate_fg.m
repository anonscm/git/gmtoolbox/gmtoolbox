function fg = gm_separate_fg (fg)
% Set a new factor graph with disconnected sub factor graphs
% Argument
%   fg: a factor graph (struct array) 
% Evaluation
%   fg: a factor graph (struct array) 

new_sfg = []; inew=0;
for i=1:length(fg.sfg)
    sfg = fg.sfg{i};
    nv = length(sfg.Vcode);
    nf = length(sfg.F);
    G = graph( [zeros(nv) sfg.BG' ; sfg.BG zeros(nf)] );
    bins = conncomp(G);
    if max(bins)==1 % sfg connected
        inew=inew+1; new_sfg{inew} = sfg;
    else    % list sub factor graphs of sfg
        for n=1:max(bins)
            sfg_n=[];
            sfg_n.Vcode = sfg.Vcode(bins(1:nv)==n);
            fn=find((bins(nv+1:end)==n));
            for f=1:length(fn)
                sfg_n.F{f}=sfg.F{fn(f)};
                Vold = sfg_n.F{f}.V; % variables labels in sfg
                V = sfg.Vcode(Vold); % variables labels in fg
                [~, Vnew] = ismember(V, sfg_n.Vcode); % variables labels in sfg_n
                sfg_n.F{f}.V = Vnew;
            end
            sfg_n.BG=setBG(length(sfg_n.Vcode) , sfg_n.F);
            inew=inew+1; new_sfg{inew} = sfg_n;
        end
    end
end
fg.sfg = new_sfg; 
end


        