function fg = gm_create_fg( Card, F, Name )
% Create a factor graph with deconnected sub factor graphs,
% giving cardinality of variables, factors and optionally names of variables.
% Let nv be the number of variables
% Argument
%    Card : cardinality of variables, vector (1 x nv) of integer
%    F : factors, cell array (1 x nf) of factor structure
%    Name : name of variables, cell array (1 x nv) of string (optional)
% Evaluation
%    fg : factor graph

fg =[]; 
% Check arguments
if nargin < 2
    disp('ERROR: at least 2 arguments must be given');    
elseif any(~isnumeric(Card)) || any(ceil(Card)~=Card) || any(Card<=0) 
    disp('ERROR: Card is not valid');
elseif nargin==3
    if length(Name)~=length(Card)
    disp('ERROR: Name must have the same length that Card');
    end    
else
    
    fg.Card = Card;
    if nargin==3; fg.Name = Name; end
    sfg.Vcode = 1:length(Card);
    sfg.F = F;
    sfg.BG = setBG(length(Card), F);
    fg.sfg{1} = sfg;
    
    [is_OK, is_disconnected, msg] = gm_check_fg( fg );
    
    if is_OK
        if is_disconnected
            disp('Warning: Disconnected sub factor graph(s) are separated'); 
            fg = gm_separate_fg( fg ); 
        end
    else
        disp('ERROR: F is not valid'); 
        disp(msg);
        fg =[];
    end
end
        