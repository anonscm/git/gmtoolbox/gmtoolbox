function fg = gm_example_Sprinkler()
% Define sprinkler toy example factor graph.
% See the Reference manual for a description.
% Variable state:  true/wet (coded 1) or false/dry (coded 2).  
% Evaluation
%   fg : a factor graph, struct array

  fg.Card = [2 2 2 2]; % cardinalities of variables
  fg.Name = {'Cloudy', 'Sprinkler', 'Rain', 'Wet'}; % names of variables

  % Define just one sub factor graph
  sfg.Vcode = [1 2 3 4]; 
  F1.V = [1];     % factor 1
  F1.T = @() [ 0.5; 0.5 ];     % function handler
  F2.V  = [1 2];   % factor 2
  F2.T = @() [ 0.5, 0.5; 0.9, 0.1];     % function handler
  F3.V  = [1 3];   % factor 3
  F3.T = @() [ 0.8, 0.2; 0.2 0.8];     % function handler
  F4.V  = [2 3 4]; % factor 4
  F4.T = @f4;     % function handler
  sfg.F ={F1, F2, F3, F4}; % factors
  sfg.BG = setBG(4, sfg.F); % matrix factors x variables
  fg.sfg{1}=sfg;
end


function cpt = f4
   cpt(1,1,:) = [ 1 0]; % Sprinkler, Rain
   cpt(1,2,:) = [ 0.9 0.1]; % Sprinkler, no Rain
   cpt(2,1,:) = [ 0.9 0.1]; % no Sprinkler, Rain
   cpt(2,2,:) = [ 0.01 0.99]; % no Sprinkler, no Rain
end 

