function evidence = gm_read_evidence(file_name, nb_vars)
% Read an .evid UAI file containing evidences
% (format description: http://www.cs.huji.ac.il/project/PASCAL/fileFormat.php)
% Arguments
%   file_name : name of the file to read
%   nb_vars : the number of variables
% Evaluation
%   evidence : evidence (array (1 x nn_vars) of integer)

evidence=[];
if exist(file_name, 'file') ~= 2
    disp('ERROR: not a such file found!');
else
    fid = fopen(file_name ,'rt');
    txt = fscanf(fid, '%c');
    txt = strsplit(txt,'\n');
    fclose(fid);
    
    evidence = zeros(1, nb_vars);
    if str2num(txt{1})==0
        disp('WARNING: no evidence provided !');
    elseif str2num(txt{1})~=1
        disp('WARNING: ONLY the first evidence sample is read !');
    else
        evid = strsplit(txt{2});
        nb_evid = str2num(evid{1});
        
        if nb_evid > nb_vars
            disp('ERROR: more evidence than variables.');
        else
            for i=1:nb_evid
                if str2num(evid{2*i})+1 > nb_vars
                    disp('ERROR: at least one variable name is greater than the number of variables.');
                else
                    evidence( str2num(evid{2*i})+1 ) = str2num(evid{2*i+1});
                end
            end
        end
    end
end
end