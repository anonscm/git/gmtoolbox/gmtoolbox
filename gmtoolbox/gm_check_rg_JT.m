function [ is_JT, is_JG, msg ] = gm_check_rg_JT( RG )
% This function tests whether the region graph given as input is 
% (i) a junction tree (ii) a junction graph. 
% Arguments
%   RG : a region graph (cell array)
%        Each sub region graph is a struct array with attributes:
%          - Vr : logical (nr x nv), with nr: number of regions, nf: number of variables
%          - Fr : logical (nr x mf), with nf: number of factors
%          - Gr : logical (nr x nr), the region graph edges)
% Evaluation
%   is_JT : A boolean which is true if all sub regions are junction tree.
%   is_JG : A boolean which is true if all sub regions are junction graph.
%   msg   : Error message (s).


is_JG = true;
is_JT = true;
msg = '';

for isrg =1:length(RG)
    sRG = RG{isrg};
    
    % CONVERT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  - rg: A region graph.
    %    - rg.regions: matrix q x (n+m). The full set of q regions of the CVM
    %    region graph.
    %    - rg.G: matrix q x q. The region graph edges.
    rg.regions = [sRG.Vr, sRG.Fr];
    rg.G = double(sRG.Gr);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % First, check whether rg is a junction graph:
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %[ Is_JG,msg ] = Check_rg_JG( rg );
    
    Vl = find(sum(rg.G,1)==0); % Large regions
    Vs = find(sum(rg.G,1));    % Small regions
    
    % Check whether rg.G is a bipartite graph
    if (nnz(rg.G(:,Vl))+nnz(rg.G(Vs,:))>0)
        % In this case, rg.G is not bipartite
        is_JG = false;
        msg = char(msg,['ERROR: RG{' int2str(isrg) '}.Gr is not a bipartite graph']);
    end
    
    % Check the small region condition
    for i=Vs
        for j = find(rg.G(:,i))
            % For every large regions connected to small region i
            if (min(rg.regions(j,:)-rg.regions(i,:))<0)
                % In this case region i is not included in region j
                is_JG = false;
                msg = char(msg,['ERROR: RG{' int2str(isrg) '}, small region ', num2str(i), ' is not included in its parent region ', num2str(j)]);
            end
        end
    end
    
    % Check subtree connectedness
    for i = 1:size(rg.regions,2)
        % All variables and factors of the region graph
        if (sum(rg.regions(:,i))>1)
            % When the condition is not satisfied, the subgraph corresponding
            % to variable i is either empty or a single vertex, thus a tree.
            nodes = find(rg.regions(:,i)); % The regions containing variable i.
            G_nodes = rg.G(nodes,nodes); % The corresponding subgraph.
            
            % A tree with k vertices has k-1 edges:
            if (nnz(G_nodes)+1 ~= length(G_nodes)) % So subgraph G_nodes connot be a tree.
                is_JG = false;
                msg = char(msg,['ERROR: RG{' int2str(isrg) '}, subgraph associated to variable ',num2str(i),' is not a tree']);
            end
            
            % Connectedness of the subgraph:
            H = G_nodes+G_nodes'+eye(length(G_nodes)); % Generation of the connectivity matrix of the subgraph.
            X = zeros(size(H,1),1);
            X(1) = 1;
            for t = 1:(length(G_nodes)-1)
                X = H*X; % We simulate the propagation of a contact process over graph G_nodes.
            end
            if (nnz(X)<length(G_nodes)) % The subgraph is disconnected.
                is_JG = false;
                msg = char(msg,['ERROR: RG{' int2str(isrg) '} is disconnected']);
            end
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Then, check whether rg is a junction tree
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if ~is_JG % The region graph is not a junction graph and so cannot be a junction tree.
        is_JT = false;
    else % The region graph is a junction graph. It remains to check that it is a connected tree.
        G_nodes = rg.G;
        
        % A tree with k vertices has k-1 edges:
        if (nnz(G_nodes)+1 ~= length(G_nodes)) % So sub-graph G_nodes connot be a tree.
            is_JT = false;
        end
        
        % Is the subgraph connected?
        H = G_nodes+G_nodes'+eye(length(G_nodes)); % Generation of the connectivity matrix of the subgraph.
        
        % Check for connectivity
        X = zeros(size(H,1),1);
        X(1) = 1;
        
        for t = 1:(length(G_nodes)-1)
            X = H*X; % We simulate the propagation of a contact process over graph G_nodes.
        end
        
        if (nnz(X)<length(G_nodes)) % The subgraph is disconnected.
            is_JT = false;
            msg = char(msg,['ERROR: RG{' int2str(isrg) '} is disconnected']);
        end
    end
end

if ~isempty(msg); msg(1,:)=[]; end
end
