function gm_write_MAR( b , file_name)
% Write a .MAR UAI file containing marginals
% (format description: http://www.cs.huji.ac.il/project/PASCAL/fileFormat.php)
% Arguments
%   b : beliefs of variables (cell array)
%   file_name : name of the file to read


fid = fopen(file_name ,'wt');
fprintf(fid, 'MAR\n1\n');
txt = int2str(length(b));
for i=1:length(b)
    txt = [ txt ' ' int2str(length(b{i})) ];
    for j=1:length(b{i}); txt = [ txt ' ' num2str(b{i}(j), '%.4f') ]; end
end

fprintf(fid, [txt '\n']);
fclose(fid);

end