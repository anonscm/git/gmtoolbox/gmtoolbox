function  rg  = gm_rg_BETHE( fg )
% Generates a junction graph rg from a factor graph fg corresponding to the Bethe method.
% The junction graph is bipartite and there is one large region for each factor 
% and one small region for each variable.
% Argument
%   fg: a factor graph (structure array)
% Evaluation
%   rg: a region graph (cell array)

rg = [];
for isfg =1:length(fg.sfg)
    sfg = fg.sfg{isfg};
    nv=length(sfg.Vcode); % number of variables of the sub factor graph
    nf=length(sfg.F); % number of factors of the sub factor graph
    
    % First, build the large regions.
    LR = [];
    % Add the factors of the subfactor graph :
    LR = [LR ; sfg.BG];
    % Compute the counting numbers
    cr = [ones(1,nf) 1-sum(LR,1)];
    LR = [LR ; eye(nv)]; % Then, we add factor i to line i of LR. LR is thus m x (n+m)
    
    % Compute the region graph for the sub factor graph:
    srg.Vr = logical(LR); % Vr(i,j) = 1 iff var j is in region i.
    srg.Fr = logical([eye(nf) ; zeros(nv,nf)]); % Fr(i,j) = 1 iff factor j is in region i.
    srg.Gr = logical([zeros(nf,nf) sfg.BG ; zeros(nv,nf+nv)]); % Gr(i,j) = 1 iff i -> j.
    srg.cr = cr; % here to have the same order of attributs
    
    rg{isfg}=srg;
end
end

