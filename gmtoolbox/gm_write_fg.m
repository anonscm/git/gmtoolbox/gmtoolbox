function gm_write_fg (fg, file_name)
% Write an .uai UAI file containing a factor graph
% (format description: http://www.cs.huji.ac.il/project/PASCAL/fileFormat.php)
% Arguments
%   fg : a factor graph (structure array)
%   file_name : name of the file to read


fid = fopen(file_name ,'wt');

fprintf(fid, ['MARKOV' '\n'] );
fprintf(fid, [int2str(length(fg.Card)) '\n'] );
card = ''; for i=1:length(fg.Card); card = [card int2str(fg.Card(i)) ' ']; end
fprintf(fid, [card '\n']);
nb_factors = 0; text_var_factors='';
for j=1:length(fg.sfg)
    nb = length(fg.sfg{j}.F); nb_factors = nb_factors + nb;
    Vcode = fg.sfg{j}.Vcode -1;% Beware in UAI format, first variable is 0 (not 1)
    for k=1:length(fg.sfg{j}.F)
        f = fg.sfg{j}.F{k}; 
        text_var_factors = [text_var_factors int2str(length(f.V)) ' '];
        for i=1:length(f.V); text_var_factors = [text_var_factors int2str(Vcode(f.V(i))) ' ']; end
        text_var_factors = [text_var_factors '\n'];
    end
end
fprintf(fid, [int2str(nb_factors) '\n'] );
fprintf(fid, [text_var_factors '\n'] );
        
for j=1:length(fg.sfg)
    for k=1:length(fg.sfg{j}.F)
        T = fg.sfg{j}.F{k}.T;
        if  isa(T, 'function_handle'); T = T();end
        fprintf(fid, int2str(numel(T)) );
        
        for i=1:numel(T)
            v_i=itov(i, fg.Card( fg.sfg{j}.Vcode( fg.sfg{j}.F{k}.V )));
            cmd = ['T(' int2str(v_i(1))]; for z=2:length(v_i); cmd = [cmd ',' int2str(v_i(z))]; end; cmd = [cmd ')'];
            fprintf(fid, [' ' num2str(eval(cmd), '%.4f')]);
        end
        fprintf(fid, '\n');
    end
end

fclose(fid);
end