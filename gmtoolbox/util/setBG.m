function BG = setBG(nv, F)
% Define matrix BG that defines variables implicated in each factor 
% Arguments
%    nv : number of variables of a (sub) factor graph
%    F  : cell array (1 x nf) of factors
% Evaluation
%    BG : matrix (nf x nv) of logical
%    BG(i,j) = true if variable i is in factor j

   BG = false(length(F),nv);
   for f=1:length(F); BG( f, F{f}.V ) = true; end
end

