function i = vtoi (v, Y)

% Tranform the vector v of integers
% defining the possibility of variation in an integer i
% Example: vtoi([3 2 1], [3 2 2]) = 11
%          vtoi([1 3 1], [2 3 2]) = 5

Y = [fliplr( cumprod( fliplr( Y(2:length(Y)) )) ) 1];
i = sum((v-1) .* Y) +1;
