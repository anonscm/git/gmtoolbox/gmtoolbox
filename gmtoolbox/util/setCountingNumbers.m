function cr = setCountingNumbers( Gr )
% Computes the counting numbers of a region graph.
% Input:
%    - Gr: a region graph boolean adjacency matrix.
% Output:
%    - cr: a vector of counting numbers


    Ar=full(adjacency(transclosure(digraph(real(Gr)))));

    cr=zeros(1,size(Ar,2)); % Counting numbers
    for r = 1:length(cr) % For all regions
        if (nnz(Ar(:,r))==0) % a root region, so cr=1
            cr(r) = 1;
        else
            Ir = find(Ar(:,r)); % The ancestor regions of r
            cr(r) = 1-sum(cr(Ir));
        end     
    end

end

