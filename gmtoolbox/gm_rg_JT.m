function rg = gm_rg_JT( fg, var_order )
% Generate a region graph rg from a factor graph fg. 
% The GBP algorithm should be exact if it runs with this region graph !
% Argument
%   fg: a factor graph (struct array) 
%   var_order : A vector (1 x nv), which is a reordering of the variables,
%   corresponding to an elimination order, and thus a junction tree.
%   Different variables orderings will provide different region graph on
%   which GBP may run more or less efficiently. However, whichever the order,
%   the result should be exact.
%   var_order will be taken into account only if fg has just one connected
%   sub factor graph. (optional, default order given by MinFill heuristic)
% Evaluation
%   rg: a region graph (cell array)

rg=[];
% Check var_order argument if provided
if nargin==2 && length(unique(var_order))<length(var_order) % There are multiple values in var_order
    disp('ERROR: Mutliple values in var_order');
elseif nargin==2 && length(var_order)~=length(fg.Card) % Incorrect number of variables
    disp('ERROR: Incorrect number of variables');
else
    
    for isfg =1:length(fg.sfg)
        sfg = fg.sfg{isfg};
        nv = length(sfg.Vcode); % number of variables of the sub factor graph

        if  nargin<2
            var_order_sfg = get_rg_MinFill( sfg ); % MinFill heuristics to compute var_order
        else
            var_order_sfg = [];
            for v=var_order
                [isIn, v_sfg] = ismember(v, sfg.Vcode);
                if isIn; var_order_sfg = [var_order_sfg v_sfg]; end
            end
        end
        % Compute large regions first.
        LR = Compute_large_regions( sfg, var_order_sfg ); % Initialization
        % Then, compute small regions as maximal intersections of any pairs of
        %  large regions, the region graph and the counts...
        rg{isfg} = Compute_small_regions( LR, nv );
    end
end
end


function [ LR,F,X ] = Compute_large_regions( fg, o )
% Generates a region graph rg from a factor graph fg and an
% ordering of variables, which is a junction tree. The GBP algorithm should
% then be exact (provided regions are small enough for the algorithm to
% run!).
% Input:
% - fg: a factor graph structure.
%
% Output:
%   - LR: A matrix of large regions, p x (n+m), issued from variable
%   elimination.

G = fg.BG; % The matrix of factors scopes 
n = size(G,2); % Number of variables
m = size(G,1); % Number of factors.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialisation for k=1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
k = 1;
F = G(:,o(k))'; % F is a line vactor of length m, with a 1 for each factor containing x_o(1).
X = max(G(find(F),:),[],1); % X is a line vector containing the variables which co-occur with var o(1) in a factor.
G = [G ; X]; % We add the new factor.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main loop.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if n>1 % We are done if there is only one variable!
    for k=2:n
        % Compute the factors which contain o(k) and no variable in
        Fk = G(:,o(k))'; % Fk contains the factors (initial and created) which contain o(k)
        Fk(1:m) = Fk(1:m) .* (1-max(F,[],1)); % We suppress all factors which already appear in F.
        F = [F ; Fk(1:m)]; % Initial factors containing o(k), not yet in F.
        
        % Compute the variables, not eliminated yet and contained in an
        % initial or created factor containing variable o(k):
        Xk = max(G(find(G(:,o(k))),:),[],1); % Those variable sharing a factor  with variable o(k).
                
        Xk(o(1:k-1)) = 0;   % We remove all variables already eliminated
        X = [X ; Xk];
        G = [G ; Xk];
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build large regions by removing those which variables are included in
% others and merge factors.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k= 2:n
    for l=1:(k-1)
        if (nnz(X(k,:).*(1-X(l,:)))==0) % In which case, Xk is a subset of Xl
            F(l,:) = F(l,:)+F(k,:); % We add the factors in Fk to these in Fl
            F(k,:) = zeros(1,m);
            X(k,:) = zeros(1,n); % We "suppress" region Rk.
        end
    end
end

I = find(sum([X F],2)); % Keep those non-empty regions.
LR = [X(I,:) F(I,:)]; % Large regions
end


function rg_out = Compute_small_regions( LR,n )
% Input:
%  - LR: Scopes of large regions.
%  - n: number of variables.
% Output:
%  - rg_out a completed region graph.

qL = size(LR,1); % Number of large regions.
SR = []; % Set of small regions, initially empty.

if qL<2 % Only one region.
    SR = [];
    Gout = [0];
    qS = 0; % No small regions
else
    % Computation of the clique weighted graph:
    CG = zeros(qL); % Clique weighted graph:
                    % CG(i,j) = |rg.regions(i,:) .* rg.regions(i,:)| if 
                    % i ~= j and CG(i,i) = 0.
    for i=1:(qL-1)
        for j = (i+1):qL
            CG(i,j) = sum(LR(i,:) .* LR(j,:));
        end
    end
    CG = CG+CG';
    
    % Computation of the maximum spanning tree:
    [MST] = Max_Spanning_Tree_Kruskal(CG);
    
    % Computation of the extended regions and regions graph, including
    % small regions:
    qS = size(MST,1); % number of small regions...
    
    % (small+large) region graph:
    Gout = zeros(qL+qS); % qL+qS is the total number of regions.
    
    % Build small regions and add edges to the region graph:
    for k=1:qS
        % We add a small region corresponding to an intersection between
        % large regions for each edge in the spanning tree:
        SR = [SR ; LR(MST(k,1),:) .* LR(MST(k,2),:)];
        
        % We add edges from the large regions corresponding to the ends of
        % edge k, to the small region corresponding to k:
        Gout(MST(k,1),qL+k) = 1;
        Gout(MST(k,2),qL+k) = 1;
    end
end

% Then we get the regions and region graph:
regions = [LR ; SR];
rg_out.Vr = logical(regions(:,1:n));
rg_out.Fr = logical(regions(:,(n+1):end));
rg_out.Gr = logical(Gout);

% And finally, the counting numbers vector:
rg_out.cr = ones(1,qL+qS) - sum(rg_out.Gr); % The count is one for every large regions and 1-#parents for each small region. 
end


function [Tree] = Max_Spanning_Tree_Kruskal(G)
% INPUT : G : Symmetric n x n matrix G with positive weights.
% OUTPUT: Tree Matrix (n-1) x 3 representing a spanning tree.
%         Tree(k,1) and Tree(k,2) are begin and end vertices of edge k  and Tree(k,3) is the weight of edge k of the Spanning tree.
    
% Start by forming the list of edges in decreasing order of weight.
Edges = []; %% Matrix of size m x 3 where m  is the number of positive weights in the upper triangle of G.
n = size(G,1);
for i=1:(n-1)
    for j=(i+1):n
        if G(i,j)>0 
            Edges = [Edges ; [i j G(i,j)]];
        end
    end
end
Edges = sortrows(Edges,3,'descend'); % in Matlab...
    
% Now build the spanning tree using Kruskal's method
Tree = []; % Initially empty spanning tree
ConnectedClasses = 1:n; % Initially, there are n connected classes, one for each vertex.
for k = 1:size(Edges,1) % Consider edges in decreasing order of weight.
    if ConnectedClasses(Edges(k,1)) ~= ConnectedClasses(Edges(k,2)) % Vertices belong to separate classes
       Tree = [Tree ; Edges(k,:)]; % Edd the edge to the spanning tree
       I = find(ConnectedClasses == ConnectedClasses(Edges(k,2))); % All vertices in the same class as the second vertex of edge k.
       ConnectedClasses(I) = ConnectedClasses(Edges(k,1)); % Both classes are united.
    end
end
end

function [var_order, fillins] = get_rg_MinFill( fg )
% This function generates a variable ordering for a factor graph fg, using
% the min-fill heuristuic.
% Input:
% - fg: a factor graph structure.
% Output:
% - var_order : A vector 1xn, which is a reodering of the variables,
% corresponding to an elimination order, and thus a junction tree.
% - fillins is the number of fill-in edges added during the variable
% elimination algorithm using order of elimination var_order.

% Initialization.
n = size(fg.BG,2);
Fills = zeros(1,n);
I=1:n;
var_order = [];
fillins = 0;

% The co-occurence graph between variables.
H = zeros(n,n); 
for f=1:size(fg.BG,1) 
    H(find(fg.BG(f,:)),find(fg.BG(f,:))) = 1; % an edge is put between every pairs of co-occuring variables.
end
    
% Main loop of elimination
while ~isempty(I)
    % Compute the number of fills for every possible variable to eliminate:
    for i=I
        Ni = find(H(i,:)); % The neighbours of i, including itself.
        HNi = H;
        HNi(Ni,Ni) = 1; % Compute the fill-in edges.
        Fills(i) = nnz(HNi-H)/2; % Compute the number of fill-in edges.
    end
    
    % Compute the variable to eliminate:
    [nfills,iloc] = min(Fills(I));
    i_to_elim = I(iloc);
    
    % Perform elimination
    Ni_to_elim = find(H(i_to_elim,:));
    H(Ni_to_elim,Ni_to_elim) = 1;
    H(i_to_elim,:) = 0;
    H(:,i_to_elim) = 0;
    I = setdiff(I,i_to_elim);
    var_order = [var_order i_to_elim];
    fillins = fillins+nfills;
end
end




