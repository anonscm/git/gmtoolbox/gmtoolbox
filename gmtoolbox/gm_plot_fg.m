function gm_plot_fg( fg , vsfg, layout )
% Plot a factor graph (a set of sub factors graphs)
% Arguments
%   fg : a factor graph (struct array)
%   vsfg : sub factor graphs to display [optional, by default all]
%   layout : a plot layout [optional, default: 'auto']
%      possible values: auto, circle, force, layered, subspace, force3, subspace3


% Check arguments
if nargin >= 2 && ( any(~isnumeric(vsfg)) || any(ceil(vsfg)~=vsfg) || ...
        any(vsfg<1) || any(vsfg>length(fg.sfg)) )
    disp('ERROR: vsfg must be a vector of sub factor graph')
elseif  nargin == 2 && ~(strcmp(layout,'auto')|| strcmp(layout,'circle')|| strcmp(layout,'force')|| ...
        strcmp(layout,'layered')|| strcmp(layout,'subspace')|| strcmp(layout,'force3')|| strcmp(layout,'subspace3'))
    disp('ERROR: layout must be auto, circle, force, layered, subspace, force3, subspace3')
else
    % set default values
    if nargin < 2; vsfg = 1:length(fg.sfg); end
    if nargin < 3; layout = 'auto'; end
    
    figure;
    for i = 1:length(vsfg)
        isfg = vsfg(i);
        sfg = fg.sfg{isfg};
        nv = length(sfg.Vcode); % number of variables of the sub factor graph
        nf = length(sfg.F); % number of factors of the sub factor graph
        
        G = graph( [zeros(nv) sfg.BG' ; sfg.BG zeros(nf)] );
        subplot(1,length(vsfg),i)
        h=plot(G,'Layout', layout);
        highlight(h,1:nv)
        highlight(h,(nv+1):nv+nf,'NodeColor','r','Marker','s')
        NodeLabel = cell(1,nv+nf);
        if isfield(fg,'Name')
            %for v=1:nv; NodeLabel{v}=fg.Name(sfg.Vcode(v)); end
            NodeLabel(1:nv)=fg.Name(sfg.Vcode);
        else
            for v=1:nv; NodeLabel{v}=int2str(sfg.Vcode(v)); end
        end
            for f=1:nf; NodeLabel{nv+f}=['f' int2str(f)]; end
            h.NodeLabel = NodeLabel;

        set(gca,'xtick',[]);set(gca,'ytick',[])
        title(['Sub factor graph ' int2str(isfg)]);
    end
    
    if isfield(fg,'evidence')
        disp(['Evidence: ' int2str(fg.evidence)])
    end
    
end
end