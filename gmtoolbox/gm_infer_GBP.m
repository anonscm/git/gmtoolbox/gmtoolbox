function [b, stop_by, br] = gm_infer_GBP ( fg , rg, Nmax, epsilon, damp, is_verbose )
% Generalize Belief Propagation algorithm (parent-to-child)
% Argument
%   fg : a factor graph (structure array)
%   rg : a region graph (cell array)
%   Nmax : maximum number of iteration (optional, default 100)
%   epsilon : stop iteration when the diffence between messages 
%             between 2 iterations is less than epsilon (optional, default 0.0001)
%   damp : Damping constant (0.0 means: take new value of message only, 
%          1.0 means: take old value of messages only) 
%          Use damping when no convergence (optional, default 0.0)
%   is_verbose : boolean, true for verbose (optional, default false)
% Evaluation
%   b : belief of variables (cell array)
%   stop_by : stop condition (cell array of string)
%   br : belief of regions (cell array)


% Default arguments and check
is_valid = true;
if nargin<2
    disp('ERROR: At least 2 arguments needed'); is_valid = false;
else
    if nargin<6; is_verbose = false; end    
    if nargin<5; damp = 0.0; end
    if nargin<4; epsilon=0.0001; end
    if nargin<3; Nmax = 100; end
end
if length(fg.sfg)~=length(rg)  
    disp('ERROR: the number of region graphs do not correspond to the number of sub factor graphs in fg'); is_valid = false;
elseif ~isnumeric(Nmax) || Nmax-ceil(Nmax)~=0 || Nmax>100000
    disp('ERROR: Nmax value not allowed'); is_valid = false;
elseif ~isnumeric(epsilon) || epsilon <=0 || epsilon > 1
     disp('ERROR: epsilon value not allowed'); is_valid = false;
elseif ~isnumeric(damp) || damp <0 || damp >= 1
     disp('ERROR: damp value not allowed'); is_valid = false;
end   
    
b=[]; stop_by=[]; br=[];
if is_valid
    for isfg =1:length(fg.sfg)
        sfg = fg.sfg{isfg};
        srg = rg{isfg};
        nv = length(sfg.Vcode); % number of variables of the sub factor graph
        nr = size(srg.Gr,1); % number of regions
        [PD, N, D, m_order] = init_variables(srg);
              
        % init messages
        m=cell(nr,nr); % messages
        for P = 1:nr % uniform distribution
            for R = find(srg.Gr(P,:)) % regions directly connected to P
                V=find(srg.Vr(R,:)); % variables of the region
                if length(V)==1; z=ones(fg.Card(sfg.Vcode(V)),1); else; z= ones(fg.Card(sfg.Vcode(V))); end
                m{P, R} = z ;%/ prod(sfg.Card(V)); % divide if normalisation by sum and not max
            end
        end
        
        %% Iterate on messages
        delta = 999999; % maximum variation of messages in one iteration
        n=0;
        while ~isempty(m_order) && delta > epsilon && n < Nmax 
            n=n+1;
            
            % Update messages
            % new_m = m; remove to be sure that new_m is updated when taken into account
            for index_PR=1:size(m_order,2)
                PR = m_order(:,index_PR);
                P=PR(1);R=PR(2);
                Array_list=[]; V_list=[]; i=0;
                % product of factors in P not in R
                for f = find(and(srg.Fr(P,:),~srg.Fr(R,:)))
                    i=i+1;
                    if isa(sfg.F{f}.T, 'function_handle'); Array_list{i} = sfg.F{f}.T();
                    else; Array_list{i} = sfg.F{f}.T; end
                    V_list{i} = sfg.F{f}.V;
                end
                % product of messages with N
                for index_NPR = find(N(index_PR,:))
                    Npr = m_order(:,index_NPR);
                    i=i+1;
                    Array_list{i} = m{Npr(1),Npr(2)};
                    V_list{i} = find( srg.Vr(Npr(2),:) );
                end
                [Array_out, V_out] = Prod_Array( Array_list, V_list );
                V_out_bool = false(1,nv); V_out_bool(V_out) = true;
                Vmarg_bool = (V_out_bool & ~srg.Vr(R,:));
                Array_list=[]; V_list=[];
                [Array_list{1}, V_list{1}]= marginalize_gm(Array_out, V_out, find(Vmarg_bool));
                i=1;
                % product of messages with D
                for index_DPR = find(D(index_PR,:))
                    %Dpr = D{P,R}
                    Dpr = m_order(:,index_DPR);
                    i=i+1;
                    Array_list{i} = 1./new_m{Dpr(1),Dpr(2)};
                    V_list{i} = find( srg.Vr(Dpr(2),:) );
                end
                [new_m{P,R}, V_out] = Prod_Array ( Array_list, V_list);
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % 28.03.2019 : When we changed order in Prod_Array, we have
                % to change it back here.
                if (length(V_out)>1)
                    [~,order_out] = sort(V_out); % Sort the variables in V_out
                    new_m{P,R} = permute(new_m{P,R},order_out); % Rearranges the dimensions of Array_list{k} in increasing order of the variables.
                end
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                % sometimes not all region variables are in new message
                V_not_present = srg.Vr(R,:); V_not_present(V_out)=false;
                if any(V_not_present)
                    if sum(srg.Vr(R,:))==1; M=zeros(sfg.Card(srg.Vr(R,:)),1);
                    else M=zeros(fg.Card(srg.Vr(R,:))); end
                    card = fg.Card(V_not_present);
                    for i=1:prod(card)
                        i_v = itov(i,card); n_i_v=1; cmd='M(';
                        for v=find(srg.Vr(R,:))
                            if V_not_present(v); cmd=[cmd int2str(i_v(n_i_v)) ',']; n_i_v= n_i_v +1;
                            else cmd=[cmd ':,']; end
                        end
                        cmd=cmd(1:(length(cmd)-1)); cmd=[cmd ') = new_m{P,R};']; eval(cmd);
                    end
                    new_m{P,R} = M;
                end
                
                %size(m{P,R})
                %size(new_m{P,R})
                %V_out
                %find(srg.Vr(R,:))
                
                new_m{P,R} = (1-damp)*new_m{P,R} + damp*m{P,R};
            end
            
            % normalize message and compute differences
            delta_m = zeros(1,size(m_order,1)); i=0;
            for PR = m_order
                P=PR(1);R=PR(2);
                new_m{P,R} =  max( 0.0001, new_m{P,R} / max(reshape(new_m{P,R},1,numel(new_m{P,R}))) );
                i=i+1; delta_m(i) = max(abs( reshape(new_m{P,R},1,numel(new_m{P,R})) ...
                    - reshape(m{P,R},1,numel(m{P,R})) ));
                
            end
            m=new_m;
            
            % Compute stop conditions
            delta = max(delta_m);
            if is_verbose; disp(['n=' int2str(n) '    delta=' num2str(delta)]); end
         end
        
        if n==Nmax; stop_by{isfg} = 'Nmax';
        else; stop_by{isfg} = 'epsilon';
        end
        
        %% Compute beliefs
        [bsfg, br{isfg}] = compute_beliefs(m, sfg, srg, PD,m_order);
        for iv=1:length(sfg.Vcode); b{sfg.Vcode(iv)}=bsfg{iv}; end
    end
    
    % Add belief for evidence
    if isfield(fg,'evidence')
        for v = find(fg.evidence~=0)
            b{v}=zeros(fg.Card(v),1);
            b{v}(fg.evidence(v))=1;
        end
    end
end
end


function [PD, N, D, m_order,E,De] = init_variables(rg)
% rg : region graph

% De: Matrix nr x nr. De(P,R) = true if region R is a descendant of region P 
% E: Matrix nr x nr. E(P,R) = true if region R is a descendant of region P or region P itself
% m_order: matrix 2 x ne. m_order(:,e) is an edge (P,R) where P = m_order(1,e) and R = m_order(2,e). 
% N: Matrix ne x ne. N(e1,e2) = 1 iff m_order(:,e1) = (P,R) and m_order(:,e2) = (I,J) such that
% I not in E(P), J in E(P), J not in E(R)
% D: Matrix ne x ne. D(e1,e2) = 1 iff m_order(:,e1) = (P,R) and m_order(:,e2) = (I,J) such that
% I in De(P), I not in E(R), J in E(R)
% PD: matrix nr x ne. PD(R,e) = 1 iff m_order(:,e) = (I,J), such that
% I in Pa(J) \ E(R) and J in De(R)

G = rg.Gr;
nr = size(G,1); % number of regions
ne = nnz(G); % number of edges in the region graph
H = G+eye(nr,nr);


% Compute E and De
E_old = H;
E = (H*E_old>0);
while any(any(E-E_old))
    E_old = E;
    E = (H*E>0);
end
De = E-eye(nr,nr);

% Compute m_order
%[I J] = find(G);
%m_order = [I'; J'];
% messages should be sent from leaves to roots:
%m_order = m_order(:,end:-1:1);
m_order =[];
while any(any(G))
    % Current leaves
    leaves = (sum(G,2)==0);
    To_leaves=any(G(:,~leaves),2);
    H=G;
    H(To_leaves,:)=0;
    [I,J]= find(H);
    m_order = [m_order [I' ; J']];
    G(H)=0;
end

if ~isempty(m_order)
    % Compute PD, N and D
    % Compute N
    Q1 = 1-E(m_order(1,:),:);
    N1 = Q1(:,m_order(1,:)');
    Q2 = E(m_order(1,:),:).*(1-E(m_order(2,:),:));
    N2 = Q2(:,m_order(2,:)');
    N = N1.*N2;
    
    % Compute D
    Q1 = De(m_order(1,:),:).*(1-E(m_order(2,:),:));
    D1 = Q1(:,m_order(1,:));
    Q2 = E(m_order(2,:),:);
    D2 = Q2(:,m_order(2,:)');
    D = D1.*D2;

    % Compute PD
    PD=(1-E(:,m_order(1,:))).*De(:,m_order(2,:));
else
    N=[];
    D=[];
    PD=[];
end
end



function [b, br] = compute_beliefs(m, fg, rg, PD, m_order)
% Arguments :
%  m : messages (cell array)
%  fg : sub factor graph (struture)
%  rg : region graph (struture)
%  PD : ... (cell array)
% Evaluation :
%  b  : belief of variables (cell array)

nr = size(rg.Gr,1); % number of regions
nv = length(fg.Vcode); % number of variable

% Find regions useful to compute b
Rv=[];
for v = 1:nv
    % Marginalise the belief of a region including the variable v
    r = find(rg.Vr(:,v)); Rv = [Rv r(end)]; % choose a leaf region
end

%% beliefs of regions
br=cell(1,nr); 
%for R = unique(Rv)  
for R = 1:nr
    Array_list=[]; V_list=[]; i=0;
    for f = find(rg.Fr(R,:)) % factors of region R
        i=i+1;
        %V_list{i} = find(fg.BG(f,:));
        V_list{i} = fg.F{f}.V;
        if isa(fg.F{f}.T, 'function_handle'); Array_list{i} = fg.F{f}.T();
        else; Array_list{i} = fg.F{f}.T; end
    end
    parents = find(rg.Gr(:,R))'; % parents of region R
    if ~isempty(parents); for P = parents 
            i=i+1;
            V_list{i} = find( rg.Vr(R,:) );
            Array_list{i} = m{P,R};
    end;end

    if ~isempty(PD)&&any(PD(R,:))
    for index_pd=find(PD(R,:))
        pd = m_order(:,index_pd);
        if ~isempty(pd)
            i=i+1;
            V_list{i} = find(rg.Vr(pd(2),:));
            Array_list{i} = m{pd(1), pd(2)};
        end
    end
    end
    
    [w, Vw] = Prod_Array( Array_list, V_list );
    Vw_bool = false(1,length(fg.Vcode)); Vw_bool(Vw) = true;
    Vmarg_bool = and(Vw_bool, ~rg.Vr(R,:)); 
    br{R} = marginalize_gm( w, Vw, find( Vmarg_bool ));     
    % normalize
    br{R} = br{R} / sum(reshape(br{R}, 1, numel(br{R})));
end 

%% beliefs of variables
% Marginalise the belief of a region including a variable
b=cell(1,nv);
for v = 1:nv
    % Marginalise the belief of a region including the variable v
    R = Rv(v); 
    Vregion_bool = rg.Vr(R,:); 
    Vmarg_bool = Vregion_bool; Vmarg_bool(v) = false;
    b{v}=marginalize_gm(br{R}, find(Vregion_bool), find(Vmarg_bool));
end
end


function [A_out, V_out] = marginalize_gm(A, V, Vmarg)
% marginalize array A associated to a set of variables V (ex: [1 3 4])
% for variables in vector Vmarg (ex [2 3])

A_out = A;
V_out = V;
if ~isempty(Vmarg)
    for v=fliplr(V)
        if ismember(v,Vmarg) % v is to be marginalized out
            ind = find(V==v);
            A_out = sum(A_out, ind);
            V_out(ind) = [];
        end
    end
    A_out=squeeze(A_out);
    if size(A_out,1)==1 && length(size(A_out))==2; A_out = A_out'; end
end
end




function [ Array_out, V_out ] = Prod_Array( Array_list, V_list )
% This function computes the products of subfunctions in f_list, of limited
% scopes in scopes_list.
% INPUT:
%    - Array_list : a list of m multidimensional arrays : f_list{k} is a
%    multidimensional array of dimension length(scopes_list{k}).
%    - V_list : A list of vectors of indices of variables.
%    V_list{k} is the list of vectors of variables in f_list{k}.
% OUTPUT:
%    - Array_out: a multidimensional array. 
%    - V_out: the scope of Array_out. It is the union of the scopes in scopes_list.

% Remove the empty factors i.e. the constants
% Matrix representation of the scopes:
m_V_list = [];
for k=length(V_list):-1:1
    if isempty(V_list{k}) % It should be removed!
        V_list(k) = []; % Removes the k-th element and does not replace it with []!
        Array_list(k) = []; % the same.
    end
end

for k=1:length(V_list)
    m_V_list(k,V_list{k}) = 1;
end

m = length(V_list); % The number of subfunctions.

% Computation of V_out:
% Initialization    
if (m==0)
    V_out=[]; % Empty scope.
    Array_out=1;
elseif (m==1) % Single function in, single function out...
    Array_out=Array_list{1};
    V_out=V_list{1};
else % At least two factors   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 14.03.2019 : Problem when the variables of the input factors are not 
    % sorted in ascending order -> Sort them!
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for k=1:m
        if length(V_list{k})>1 % At least two variables
            [V_list{k},order_k] = sort(V_list{k}); % Sort the variables in V_list{k}
            Array_list{k} = permute(Array_list{k},order_k); % Rearranges the dimensions of Array_list{k} in increasing order of the variables.
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % New computation of V_out, exploiting m_V_list
    V_out = find(sum(m_V_list,1));
    
    Domains = zeros(m,numel(V_out)); % Domains is a matrix for which each line will represent the domain sizes of the variables in the subfactor.
    for k=1:m
        size_fk=size(Array_list{k});
        Ik = find(m_V_list(k,V_out)); % The variables in fk among the variables in V_out
        if numel(Ik) > 1 % at least two dimensions in f_list{k}
            Domains(k,Ik) = size_fk;
        elseif numel(Ik) == 1 % Single dimension in f_list{k}
            Domains(k,Ik) = prod(size_fk);
        end
    end    
    
    % In each column, the non-zero elements should all be identical!
    dmax = max(Domains,[],1);
    Z = (repmat(dmax,m,1)-Domains).*Domains; % Z is a zero matrix except if there is a variable with two different domain sizes in different factors
    error = any(any(Z));

    if error
       disp('ERROR in Prod_Array: Incompatible domains');        
       Array_out=[];
    else
        % Now, we can compute Array_out, using function Redim and operator .*
        if size(Domains,2)>1 % In this case, everything is fine.
            Array_out = ones(max(Domains,[],1));
        else % We must take care of the case where there is a single variable in Domains : ones(d) will not do!
            Array_out = ones(max(Domains,[],1),1); % In this case, Array_out is a vector!
        end
        for k=1:m
            Array_out = Array_out .* Redim(Array_list{k},V_list{k},V_out,max(Domains,[],1));
        end
    end
end
end


function [ f_out ] = Redim( f,A,B,dims )
% This function extends the scope of f from A to a superset B of A, while
% leaving the values unchanged (new dimensions have singleton domains.
% INPUT:
%   - f: A multidimensional array, which variables are indexed in A.
%   - A: Vector of variables of f indices.
%   - B: A superset of A.
%   - dims is a vector of length numel(B). its elements are the sizes of
%   the domains and f_out, which should be compatible with f...
% OUTPUT:
%   - f_out: A multidimensional array, which variables are indexed in B.
%   all dimensions not in A are that of dims, so that the elements of f_out are exactly those of f replicated.

I_B = [];
I_B(1,B) = 1; % I_B is a vector of length 1 x max(B) and I_B(1,i) = 1 iff variable i belongs to B.
I_B=logical(I_B);
I_A = zeros(1,numel(I_B));
I_A(A) = 1; % I_A is a vector of length 1 x max(B) and I_A(1,i) = 1 iff variable i belongs to A.

%if ~isempty(setdiff(A,B)) % Houston, we have a problem: A is not a subset of B!
if any(I_A.*(1-I_B)) % Houston, we have a problem: A is not a subset of B!
    f_out=[];
    pb='problem 1'
%elseif isempty(A) % Coquinou, f=[]!
elseif ~any(I_A) % Coquinou, f=[]!
    f_out = [];
else % Relieved!
    % Let us compute the sizes 
    %size_f = size(f); % Domains of variables of f.
    s_out = ones(1,numel(B)); 
    %[~,A_in_B] = ismember(A,B); % A_in_B contains the indices of elements of A in B.
    A_in_B = find(I_A(I_B)); % A_in_B contains the indices of elements of A in B.
    if numel(A_in_B)>1 % That is f is a function of more than one variable
        s_out(A_in_B) = size(f);
    else % That is f is a function of a single variable, but still, size(f) has two elements!!
        s_out(A_in_B) = numel(f); % Yes, one of the two elements of size(f) is 1 ;-)
    end
    % In this way, s_out represent the domains of f_out, with singleton
    % dimensions for variables which are not in f...
    
    % Now lets fill in the elements of f in f_out :
    if numel(s_out)>1 % All is fine when there are at least 2 elements in B
        f_out = reshape(reshape(f,prod(size(f),1)),s_out); % Here is the miracle...
    else
        f_out = reshape(reshape(f,prod(size(f),1)),s_out,1); % This to get a vector when s_out is a single number.
    end
    
    % OK, we are left with inflating the dimensions to stick to dims...
    % But first check that dims is compatible with f!
    if (numel(A_in_B)>1)&&any(dims(A_in_B)-size(f))
        f_out=[];
        pb='problem 2'
    else % OK, then repmat!
        inflate = dims;
        inflate(A_in_B) = 1;
        f_out = repmat(f_out,inflate);
    end
        
end
end

