function bt = gm_plot_belief_evolution ( V, S, i_start, i_end, fg, rg, epsilon, damp )
% For a set of variables and state, compute evolution of belief during GBP
% BEWARE: due to recurrent call to gm_infer_GBP, execution time may be very long !
% Argument
%   V : variables (vector of integer)
%   S : states (vector of integer)
%   i_start : first maximum number of iterations 
%   i_end : last maximum number of iterations
%   fg : factor graph (structure array)
%   rg : region graph (cell array)
%   epsilon : stop iteration when the diffence between messages 
%             between 2 iterations is less than epsilon.
%             real in [0 1[ (argument of gm_infer_GBP)
%   damp : Damping constant (0.0 means: take new value of message only, 
%          1.0 means: take old value of messages only) (argument of gm_infer_GBP)
%          real in [0 1] (argument of gm_infer_GBP)
% Evaluation
%    bt : beliefs of variables for each required iterations


% Check arguments
if any(V<1) || any(V>length(fg.Card)) || any(~isnumeric(V)) || any(ceil(V)~=V)
    disp('ERROR: V not correct')
elseif any(S<1) || any(S>fg.Card(V)) || any(~isnumeric(S)) || any(ceil(S)~=S)
    disp('ERROR: S not correct')
elseif ~isnumeric(i_start) || i_start<1 || ceil(i_start)~=i_start
    disp('ERROR: i_start not correct')
elseif ~isnumeric(i_end) || i_end<1 || i_end-i_start<=0 || ceil(i_end)~=i_end
    disp('ERROR: i_end not correct')
else
    % compute beliefs for all Nmax
    for i=i_start:i_end
        eval(['b' int2str(i) '= gm_infer_GBP( fg, rg, ' int2str(i) ',' num2str(epsilon) ',' num2str(damp) ');']);
    end

    bt=zeros(i_end-i_start+1, length(V));
    for k=1:length(V)
        v=V(k);
        s=S(k);
        % compute belief
        for i=i_start:i_end; eval(['bt(' int2str(i) ', ' int2str(k)  ') = b' int2str(i) '{' int2str(v) '}(' int2str(s)  ');']); end
    end
    
    figure; plot(bt); xlabel('iteration'); ylabel('b\{V\}(S)');
    text=[]; for k=1:length(V); text=[text; ['pair' int2str(k)]];end
    legend(text);
end
end