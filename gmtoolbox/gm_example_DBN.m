function fg = gm_example_DBN(nv, t, Name)
% Generate a factor graph for a DBN (see Reference Manual for a description)
% Arguments
%     nv : number of variables
%     t  : time step
%     Name : name of variables
%     NS : global variable, number of state of each variable
% Evaluation
%   fg : a factor graph, struct array
% Example: global NS; NS=3; fg = gm_example_DBN(4,5)

fg =[];
global NS  % number of state of variables
if ~exist('NS')
    disp('ERROR: NS has to be defined as a global variable');
elseif nargin > 2 && length(Name)~=nv*(t+1)
        disp('ERROR: A name must be specified for each variable')
else
    rng(1);
    
    if  nargin > 2; fg.Name = Name; end
    fg.Card = NS*ones(1,(t+1)*nv);
    
    % Define just one sub factor graph
    sfg.Vcode = 1:length(fg.Card);
    nf = t*nv +nv; % factors between time steps and initial ones
    sfg.F=cell(1, nf);
    for k=1:t
        for v=1:(nv-1)
            F.V=[(k-1)*nv+v, (k-1)*nv+v+1, k*nv+v];
            F.T = get_cpt();
            sfg.F{(k-1)*nv+v}=F;
        end
        % last variable
        F.V=[nv*k, (k-1)*nv+1, (k+1)*nv];
        F.T = get_cpt();
        sfg.F{nv*k}=F;
    end
    for k=1:nv
        F.V=k;
        F.T=ones(NS,1)/NS;
        sfg.F{k+nv*t}=F;
    end
    sfg.BG = setBG(length(sfg.Vcode), sfg.F); % matrix factors x variables
    
    fg.sfg{1}=sfg;
end
end


function cpt = get_cpt()
    global NS
    cpt = rand(NS*ones(1,3));
    for i1=1:NS; for i2=1:NS; k=cpt(i1,i2,:); cpt(i1,i2,:)= k/sum(k); end;end
end
