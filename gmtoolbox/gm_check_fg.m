function [is_OK, is_disconnected, msg] = gm_check_fg( fg )
% Check the validity of a factor graph
% Argument
%   fg : a factor graph (structure array)
% Evaluation
%   is_OK : true if fg is valid (boolean)
%   is_disconnected : true if at least one sub factor graph of fg is disconnected (boolean)
%   msg : detected non validity

is_OK = true;
is_disconnected = false;
msg='';

if any(fg.Card<2)
    msg=char(msg,['ERROR: All cardinal of variables must be upper than 1']);
else
     % Check that all variables appear in sub factor graphs
     is_evidence = false(1,length(fg.Card));
     if  isfield(fg,'evidence'); is_evidence( fg.evidence~=0 ) = true; end
     is_in_subfg = false(1,length(fg.Card)); 
     for isfg =1:length(fg.sfg); is_in_subfg(fg.sfg{isfg}.Vcode)=true; end; 
     if any(is_evidence & is_in_subfg)
         msg=char(msg,'ERROR: At least one evidence is still in a sub factor graph(s)');
     end
     if any(~(is_evidence | is_in_subfg))
         msg=char(msg,'ERROR: At least one variable not an evidence or in a sub factor graph(s)');
     end
    
    if isfield(fg,'evidence')
        if length(fg.evidence)~=length(fg.Card)
            msg=char(msg,'ERROR: length of evidence not correct');
        else
            if any(~isnumeric(fg.evidence)) || any(ceil(fg.evidence)~=fg.evidence) || ...
                    any(fg.evidence<0) || any(fg.evidence>fg.Card)
                msg=char(msg,'ERROR: at least one value of evidence not correct');
            end
        end
    end
    
    % Check that all (i) variables appear in sub factor graphs
    % (ii)  sub factor graphs are disconnected (iii) each sub factor graph is valid
    list_fg_V = [];
    for isfg =1:length(fg.sfg)
        sfg = fg.sfg{isfg};
        nv = length(sfg.Vcode); % number of variables of the sub factor graph
        nf = length(sfg.F); % number of factors of the sub factor graph
        list_sfg_V =[];
        
        is_alone = true(1,nv); for f=1:nf; is_alone(sfg.F{f}.V)=false; end
        if any(is_alone)
            msg=char(msg,['ERROR: sfg{' int2str(isfg) '} all variables of the sub factor graph are not evidence or in factors']);
        else
            if max(sfg.Vcode)>length(fg.Card)
                msg=char(msg,['ERROR: sfg{' int2str(isfg) '}.Vcode define impossible variables']);
            else
                if length(sfg.Vcode)~=length(unique(sfg.Vcode))
                    msg=char(msg,['ERROR: sfg{' int2str(isfg) '} has repetition in Vcode']);
                end
                if any(ismember(sfg.Vcode, list_fg_V))
                    msg=char(msg,['ERROR: sfg{' int2str(isfg) '} is connected whith a preceeding sub factor graph']);
                end
                list_fg_V = [list_fg_V sfg.Vcode];
                if ~isnumeric(sfg.Vcode) || any(ceil(sfg.Vcode)~=sfg.Vcode) || ...
                        any(sfg.Vcode<1) || any(sfg.Vcode>length(fg.Card))
                    msg=char(msg,['ERROR: sfg{' int2str(isfg) '}.Vcode not correct']);
                end
                if any(size(sfg.BG)~=[nf, nv]) || ~islogical(sfg.BG)
                    msg=char(msg,['ERROR: sfg{' int2str(isfg) '}.BG not correct']);
                else
                    for i=1:nf
                        f = sfg.F{i};
                        list_sfg_V = [list_sfg_V f.V];
                        if ~isnumeric(f.V) || any(ceil(f.V)~=f.V) || any(f.V<1) || ...
                                any(f.V>nv)
                            msg=char(msg,['ERROR: sfg{' int2str(isfg) '}.F{' int2str(i) '}.V not correct']);
                        else
                            if isa(sfg.F{1}.T, 'function_handle'); T = f.T();
                            else; T = f.T; end
                            t=size(T); if t(end)==1 && length(f.V)==1; t=t(1); end
                            
                            if ~isnumeric(T) || any(reshape(T,1,numel(T))<0) || ...
                                    any(isnan(reshape(T,1,numel(T)))) || any(t~=fg.Card(sfg.Vcode(f.V)))
                                % pb when last of  variable sfg.Vcode(f.V) has cardinality 1 !
                                msg=char(msg,['ERROR: sfg{' int2str(isfg) '}.F{' int2str(i) '}.T not correct']);
                            end
                        end
                    end
                    
                    if any(~ismember(1:length(sfg.Vcode),unique(list_sfg_V)))
                        msg=char(msg,['ERROR: sfg{' int2str(isfg) '} at least one variable of Vcode not found in factors']);
                    end
                    
                    G = graph( [zeros(nv) sfg.BG' ; sfg.BG zeros(nf)] );
                    bins = conncomp(G);
                    if max(bins)>1
                        disp(['WARNING:  sfg{' int2str(isfg) '} is disconnected']);
                        is_disconnected = true;
                    end
                end
            end
        end
    end
     
end

if ~isempty(msg); is_OK = false; msg(1,:)=[]; end
end
