function fg = gm_example_Ecology()
% Generate a factor graph for a toy ecological example using gm_example_DBN function
% (see QuickStart or the Reference Manual for a description)
% Evaluation
%   fg : a factor graph, struct array

global NS; NS=3; % 3 states : population low(1) / normal(2) / high(3)
nv = 4; % 4 species
t = 2; % 2 time steps
Name = {'P¹','Z¹','K¹','M¹','P²','Z²','K²','M²','P³','Z³','K³','M³'};

fg = gm_example_DBN(nv, t, Name); % the network is a DBN
% change transition arrays in factors linking time steps
for f=1:8
    fg.sfg{1}.F{f}.T = @() setTransition;
end
% Initial states of populations are likely normal
for i=9:12; fg.sfg{1}.F{i}.T=[0.25;0.5;0.25];end
end


function T = setTransition()
% transition matrix are defined by T(V^t, v^t, V^{t+1})

% The transition of V^t to V^{t+1} in one state is defined using 4 probabilities
p1 = 0.5; % x 1  (0.7)
p2 = 0.2; % x 2  (0.2)
p3 = 0.05; % x 1 (0.005)
p4 = 0.01; % x 5 (0.001)

Tlow = [p1 p2 p4;  % T(V^t, v^t, Vt+1 = 1})
        p2 p3 p4;
        p4 p4 p4];
Tnormal =  [p4 p4 p4;  % T(V^t, v^t, Vt+1 = 2})
            p2 p1 p2;
            p4 p4 p3];
Thigh = [p4 p4 p4;   % T(V^t, v^t, Vt+1 = 3})
         p4 p3 p2;
         p4 p2 p1];

T(:,:,1)=Tlow;
T(:,:,2)=Tnormal;
T(:,:,3)=Thigh;
end
