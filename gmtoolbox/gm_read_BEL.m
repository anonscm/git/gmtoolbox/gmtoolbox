function br = gm_read_BEL( file_name, fg, rg)
% Read a .BEL UAI file containing beliefs of the regions defined in rg
% (format description: http://www.cs.huji.ac.il/project/PASCAL/fileFormat.php)
% BEWARE: ONLY the first belief sample is read !
% Arguments
%   file_name : name of the file to read
%   fg : a factor graph (structure array)
%   rg : a region graph (cell array)
% Evaluation
%   br : belief of regions (cell array)

br = {};
if exist(file_name, 'file') ~= 2
    disp('ERROR: not a such file found!');
else
    fid = fopen(file_name ,'rt');
    txt = fscanf(fid, '%c');
    txt = strsplit(txt,'\n');
    fclose(fid);
    
    if ~strcmp(txt{1},'BEL')
        disp('ERROR: not a BEL file !');
    elseif str2num(txt{2})~=1
        disp('WARNING: ONLY the first belief sample is read !');
    else
        BEL = strsplit(txt{3});
        nb_rg = str2num(BEL{1});
        sumrg=0;for isrg=1:length(rg); sumrg=sumrg+size(rg{isrg}.Gr,1);end
        if nb_rg~=sumrg
            disp('ERROR: the number of cliques is different of the total number of regions in rg !');
        else
            br_tmp = cell(1, nb_rg);
            cpt = 2;
            for irg=1:nb_rg
                card = str2num(BEL{cpt}); cpt=cpt+1;
                br_tmp{irg} = zeros(card,1);
                for j=1:card; br_tmp{irg}(j) = str2num(BEL{cpt}); cpt=cpt+1; end
            end
            
            br=cell(1,length(rg));
            irg_tmp=1;
            for isrg=1:length(rg)
                for irg=1:size(rg{isrg}.Gr,1)
                    card = fg.Card(fg.sfg{isrg}.Vcode(rg{isrg}.Vr(irg,:)));
                    if length(card)==1; card = [card 1]; end
                    T = zeros(card);
                    if numel(T)~=length(br_tmp{irg_tmp})
                        disp(['ERROR: the number of belief in sub factor graph ' int2str(isrg) ' region '  int2str(irg) ' is not correct.']);
                    else
                        for k=1:length(br_tmp{irg_tmp})
                            v_k=itov(k, size(T));
                            cmd = ['T(' int2str(v_k(1))]; for z=2:length(v_k); cmd = [cmd ',' int2str(v_k(z))]; end; cmd = [cmd ')'];
                            eval([cmd '= br_tmp{irg_tmp}(k);']);
                        end
                        br{isrg}{irg}=T;
                        irg_tmp = irg_tmp + 1;
                    end
                end
            end
        end
    end
end
end