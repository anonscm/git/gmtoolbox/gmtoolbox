function fg = gm_example_CHMM( N, T)
% Generate a factor graph with a square grid of N variables evolving during T periods
% (see QuickStart or Reference Manual for a description)
% Arguments
%   N : number of variables [square value] (default value: 9)
%   T : number of period [integer greater than 0] (default value: 2)
% Evaluation
%   fg : a factor graph, struct array

% Set default arguments and check them
if nargin < 2; T=3; end
if nargin < 1, N=9; end
if ~isnumeric(N) || ceil(N)~=N || ceil(sqrt(N))~=sqrt(N); disp('ERROR: bad value for N');
elseif ~isnumeric(T) || ceil(T)~=T || T<1 ; disp('ERROR: bad value for T');
else
    %% Init parameters
    f_p = 0.05; % p(Obs=1|X=0)
    f_n = 0.1;  % p(Obs=0|X=1)
    epsilon = 0.15; % probability of contamination from outside the landscape
    rho = 0.2; % probability that a pest spreads from an infected field to a neighbor one
    nu = 0.5; % probability that an infected field remains infected
    I=0:4; % possible number of infected neighbors
    P1 = epsilon + (1-epsilon)*(1-(1-rho).^I);
    P0 = nu + (1-nu)*(epsilon+(1-epsilon)*(1-(1-rho).^I));
    
    n=sqrt(N);
    % First N*(T+1) state variables, then N*T observation variables
    nv_state = N*(T+1); nv_obs = N*T;
    fg.Card=2*ones(1, nv_state + nv_obs );
    nv = length(fg.Card); % number of variables

    % Define different variables (nodes)
    V_corners = [1 n N-(n-1) N]; % corners variables
    V_high = 2:(n-1); % side up variables
    V_left = (n+1):n:(N-n-(n-1)); % side left variables
    V_right = (2*n):n:((n-1)*n); % side right variables
    V_down = (N-(n-2)):(N-1); % side down variables
    V_center_bool = true(1,N); V_center_bool([ V_corners V_high V_left V_right V_down])=false;
    V_center = find(V_center_bool); % variable in the center of the square

    nf=0; % number or factors
    % Define factors for initial state variable 
    for v=1:N 
        nf=nf+1; 
        sfg.F{v}.V=v;
        sfg.F{v}.T=[0.5; 0.5]; % no apriori on initial states
    end
    % Define factors between state variables for t>0
    % Define variables of factors
    for t=1:T
        % order of factors variables : state the preceding time, state of
        % neighbors the preceeding time, state the current time
        % corners nodes
        nf=nf+1; sfg.F{nf}.V=[(t-1)*N+1, (t-1)*N+1+1, (t-1)*N+1+n, t*N+1]; % up left
        nf=nf+1; sfg.F{nf}.V=[(t-1)*N+n, (t-1)*N+n-1, (t-1)*N+n+n, t*N+n];   % up right
        nf=nf+1; sfg.F{nf}.V=[(t-1)*N+N-(n-1)+1, (t-1)*N+N-(n-1)-n, (t-1)*N+N-(n-1), t*N+N-(n-1)];   % down left
        nf=nf+1; sfg.F{nf}.V=[(t-1)*N+N, (t-1)*N+N-n, (t-1)*N+N-1, t*N+N]; % down right
        % side nodes
        for v=V_high; nf=nf+1; sfg.F{nf}.V=[(t-1)*N+v, (t-1)*N+v-1, (t-1)*N+v+1, (t-1)*N+v+n, t*N+v];end
        for v=V_left; nf=nf+1; sfg.F{nf}.V=[(t-1)*N+v, (t-1)*N+v-n, (t-1)*N+v+1, (t-1)*N+v+n, t*N+v];end
        for v=V_right; nf=nf+1; sfg.F{nf}.V=[(t-1)*N+v, (t-1)*N+v-n, (t-1)*N+v-1, (t-1)*N+v+n, t*N+v];end
        for v=V_down; nf=nf+1; sfg.F{nf}.V=[(t-1)*N+v, (t-1)*N+v-n, (t-1)*N+v-1, (t-1)*N+v+1, t*N+v];end
        % central nodes
        for v=V_center; nf=nf+1; sfg.F{nf}.V=[(t-1)*N+v, (t-1)*N+v-n, (t-1)*N+v-1, (t-1)*N+v+1, (t-1)*N+v+n, t*N+v];end
    end
    
    % Define tables of factors
    T4=zeros(2,2,2,2);T5=zeros(2,2,2,2,2);T6=zeros(2,2,2,2,2,2);
    for v1=1:2 % first neighbor
        for v2=1:2 % second neighbor
            I=(v1-1)+(v2-1); % number of infected fields when 2 neighbors
            T4(:,v1,v2,:)=[1-P1(I+1), P1(I+1);
                           P0(I+1),   1-P0(I+1)];
            for v3=1:2 % third neighbor
                I=(v1-1)+(v2-1)+(v3-1); % number of infected fields when 2 neighbors
                T5(:,v1,v2,v3,:)=[1-P1(I+1), P1(I+1);
                                 P0(I+1),   1-P0(I+1)];
                for v4=1:2 % fourth neighbor
                    I=(v1-1)+(v2-1)+(v3-1)+(v4-1); % number of infected fields when 2 neighbors
                    T6(:,v1,v2,v3,v4,:)=[1-P1(I+1), P1(I+1);
                                       P0(I+1),   1-P0(I+1)];
                end
            end
        end
    end
    for v=(N+1):nv_state
        if length(sfg.F{v}.V)==4; sfg.F{v}.T=T4;
        elseif length(sfg.F{v}.V)==5; sfg.F{v}.T=T5;
        else; sfg.F{v}.T=T6;
        end
    end
   
    % Define factors for observation variables
    for v=1:nv_obs
        nf=nf+1; 
        sfg.F{nf}.V = [v+N, nv_state+v];
        sfg.F{nf}.T = [1-f_n  f_n; 
                       f_p    1-f_p];
    end   

    sfg.Vcode = 1:length(fg.Card);

    %% Compute utility attribut BG : factors x variables
    sfg.BG = setBG( nv, sfg.F);
    
    fg.sfg{1} = sfg;
end
         

             
             
