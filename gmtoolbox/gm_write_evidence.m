function gm_write_evidence( evidence, file_name )
% Write an .evid UAI file containing evidences
% (format description: http://www.cs.huji.ac.il/project/PASCAL/fileFormat.php)
% Arguments
%   evidence : evidence (array (1 x nn_vars) of integer)
%   file_name : name of the file to read


v=find(evidence ~= 0);
if isempty(v)
    disp('ERROR: no evidence provided.');
else
    fid = fopen(file_name ,'wt');
    fprintf(fid, ['1' '\n'] );
    txt = int2str(length(v));
    for i=1:length(v); txt = [ txt ' ' int2str(v(i)-1) ' ' num2str(evidence(v(i)), '%.4f')]; end
    fprintf(fid, [txt '\n']);
    fclose(fid);
end
end
