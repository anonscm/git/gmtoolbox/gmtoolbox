function gm_plot_rg( rg , vsrg, layout )
% Plot a region graph (possibly several sub region graphs)
% Arguments
%   rg : a region graph (cell array)
%   vsrg : sub region graphs to display [optional, by default all]
%   layout : a plot layout [optional, default: 'auto']
%      possible values: auto, circle, force, layered, subspace, force3, subspace3


if nargin < 2; vsrg = 1:length(rg); end
if nargin < 3; layout = 'auto'; end

figure;
for i=1:length(vsrg)
    isrg=vsrg(i);
    srg = rg{isrg};
    nr = size(srg.Gr,1); % number of regions of the sub region graph

    G = digraph( srg.Gr );
    subplot(1,length(vsrg),i)
    h=plot(G,'Layout', layout);
    highlight(h,1:nr)
    highlight(h,1:nr,'NodeColor','black','Marker','s')
    set(gca,'xtick',[]);set(gca,'ytick',[])
    title(['Sub region graph ' int2str(isrg)]);
end

end