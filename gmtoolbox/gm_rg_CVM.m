function [rg, kr] = gm_rg_CVM(fg, regions)
% Compute with the Cluster Variation Method a region graph
% Arguments
%    fg : a factor graph (struct array)
%    regions : sub regions (cell array of nr x (nv+nf) matrix, optional)   
% Evaluation
%    rg : a region graph (cell array)
%    kr : Vector of regions levels

rg = []; kr=[];
if nargin==2 && length(fg.sfg)~=length(regions)
    disp('ERROR: regions must be provided for each sub factor graph of fg');
else
    for isfg=1:length(fg.sfg)
        sfg = fg.sfg{isfg};
        nf=length(sfg.F); % number of factors of the sub factor graph
        nv=length(sfg.Vcode);
        
        if nargin==1  % extract regions from sfg
            R=false(nf, nv);
            for f=1:nf; R(f,sfg.F{f}.V)=true; end
            R = logical([R eye(nf)]);
            % remove regions whose variables are includes in an other region
            % and add the factor to all regions that contain variables
            ir=1;
            is_pursue = true;
            while is_pursue   
                index=1:size(R,1);
                lr_modif=find( sum(R(index, R(ir,1:nv)),2) == sum(R(ir,1:nv),2 ) );
                if all(lr_modif == ir) % keep region
                    if ir==size(R,1); is_pursue = false; else; ir=ir+1; end
                else
                    % at least one other region has the same set of variables
                    f=nv+find(R(ir,nv+1:end));
                    lr_modif(lr_modif==ir)=[];
                    %%  R(lr_modif(1),f)=true; if we would like to set the factor in just 1 region
                    R(lr_modif,f)=true; % the factor is set in every 'upper' region
                    R(ir,:)=[];
                    if ir>=size(R,1); is_pursue = false; end
                end
            end
        else
            R = logical(regions{isfg});
        end
        
        [R,Gr,kr] = Clusters(R);
        
        % Region graph of the sub factor graph
        srg.Vr = logical(R(:, 1:nv));
        srg.Fr = logical(R(:, (nv+1):end));
        srg.Gr = logical(Gr);
        srg.cr = setCountingNumbers(Gr);   % Compute counting numbers
        
        rg{isfg}=srg;
    end
end
end

% Clusters computation
function [R_out, G, kr] = Clusters(R)
% This function computes the clusters obtained by successive intersections
% of regions in R

% R : binary matrix m x n. R(i,j) = 1 iff variable j belongs to region i.
% R_out : binary matrix q x n. The q regions are obtained by intersections
% of regions in R, removing redundant regions as in the CVM method.
% G : Binary matrix of size q x q. G(k,l) = 1 iff region k is a superregion
% of region l
% kr : Vector of regions levels

% First, suppress subregions of R
R = Purge_subregions(R);
G = zeros(size(R,1),size(R,1)); % No links between regions in R0
kr = zeros(size(R,1),1);
k=0;

R_next = Intersections(R); % Computes intersections of pairs of regions of R, with subregions purged out.
if ~isempty(R_next)
    R_next = Purge_equal(R_next,R); % We suppress from R_next those regions which are already in R.
    R_next = Purge_subregions(R_next); % We suppress those regions of R_next which are subregions of other regions in R_next.
end

while ~isempty(R_next) % There still are intersection regions
    k=k+1;
    
    % Update G
    H = ((R_next*(ones-R'))==0)'; % H(i,j) = 1 iff R(i,:) is a superset of R_next(j,:).
    K = G*H>0; % K(i,j) = 1 iff there is a path of length 2 between i and j
    H = H.*(ones-K); % Now, H contains no more shortcuts
    G = [G H ; zeros(size(R_next,1),size(R,1)+size(R_next,1))];
    
    % Update R
    R = [R ; R_next];
    kr = [kr ; k*ones(size(R_next,1),1)];
    R_next = Intersections(R);
    if ~isempty(R_next)
        R_next = Purge_equal(R_next,R);
        R_next = Purge_subregions(R_next);
    end
end

R_out = R;
end

% Modif 28.03.2019
function R_out = Purge_subregions(R)
% Suppress the subregions of R.
% First consider only unique regions
R = unique(R,'rows');
% Then, compute the inclusion graph
Q = Subregions(R);
Q = Q.*(ones-Q'); % Now Q(i,j)=1 if and only if i<>j and Ri subset of Rj.
I = find(sum(Q,2)); % I contains the indices of the regions of R which are redundant
R_out = R;
if ~isempty(I)
    R_out(I,:) = [];
end
end

function [Q_sub] = Subregions(R)
% This function gives all subregions of R
Q = R*(ones-R'); % Q12(i,j) is the cardinal of the intersection of R1(i,:) and the complementary of R2(j,:)
Q_sub = (Q==0); % R12(i,j) = 1 iff R1(i,:) is included in R2(j,:)
end

function [R_out] = Purge_equal(R1,R2)
% This function suppresses from R1 those regions which appear in R2.
R_out=R1;
I = ismember(R1,R2,'rows');
if ~isempty(I)
    R_out(I,:)=[];
end
end


function [R_out] = Intersections(R)
% % R_out contains the intersections of distinct subregions of R.
R = unique(R,'rows');
nr = size(R,1);
R_out=[];
for i=1:nr-1
    R_out = [R_out ; repmat(R(i,:),nr-i,1).*R((i+1):nr,:)];
end
R_out = unique(R_out,'rows');
R_out(find(sum(R_out,2)==0),:)=[];
if ~isempty(R_out)
    R_out = Purge_equal(R_out,R);
end
%R_out = Purge_subregions(R_out);
end




        