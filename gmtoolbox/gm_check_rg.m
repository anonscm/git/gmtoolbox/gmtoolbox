function [is_OK, msg] = gm_check_rg( rg, fg )
% Check that (i) each sub factor graph has an associated sub region graph
% (ii) each sub region graph is valid
% Argument
%   rg : a region graph (cell array)
%   fg : a factor graph (structure array)
% Evaluation
%   is_OK : true if fg is valid (boolean)
%   is_disconnected : true if at least one sub factor graph of fg is disconnected (boolean)
%   msg : detected non validity

is_OK = true;
msg='';

if length(rg)~=length(fg.sfg)
    msg = char(msg,'ERROR: each sub region graph is not associated to a sub factor graph');
else
    for isrg =1:length(rg)
        srg = rg{isrg};
        nr = size(srg.Vr,1); % number of regions in the sub region graph
        sfg=fg.sfg{isrg};
        nv = length(sfg.Vcode); % number of variables of the associated sub factor graph
        nf = length(sfg.F); % number of factors of the associated sub factor graph
        
        if nr~=size(srg.Fr,1) || any(size(srg.Gr)~=[nr nr])
            msg = char(msg,['ERROR: rg{' int2str(isrg) '}: number or regions varies']);
        end
        if size(srg.Vr,2)~=nv
            msg = char(msg,['ERROR: rg{' int2str(isrg) '}: number of variables varies']);
        end
        if size(srg.Fr,2)~=nf
            msg = char(msg,['ERROR: rg{' int2str(isrg) '}: number of factors varies']);
        end
        if ~islogical(srg.Vr)
            msg = char(msg,['ERROR: rg{' int2str(isrg) '}: Vr not correct (must be logical)']);
        end
        if ~islogical(srg.Fr)
            msg = char(msg,['ERROR: rg{' int2str(isrg) '}: Fr not correct (must be logical)']);
        end
        if ~islogical(srg.Gr)
            msg = char(msg,['ERROR: rg{' int2str(isrg) '}: Gr not correct (must be logical)']);
        end
        if any(sum(srg.Vr,2)==0)
            msg = char(msg,['ERROR: rg{' int2str(isrg) '}: at least one region has no variable']);
        end
        if any(sum(srg.Vr)==0)
            msg = char(msg,['ERROR: rg{' int2str(isrg) '}: at least one variable is not in a region']);
        end
        is_pb=false;
        for r=1:nr
            if sum(srg.Fr(r,:))~=0 % a factor at least in the region r
                Vf_bool = false(1,nv);
                for f = find(srg.Fr(r,:)); Vf_bool(sfg.F{f}.V)=true; end
                if any(Vf_bool & ~srg.Vr(r,:)); is_pb=true; end
            end
        end
        if is_pb
            msg = char(msg,['ERROR: rg{' int2str(isrg) '}: at least one sub region do not have all of its variables in the region factors']);
        end
        if isfield(srg,'cr')
            is_pb=false;
            for f=1:nf; if sum(srg.cr(srg.Fr(:,f)))~=1; is_pb=true; end; end
            if is_pb
                msg = char(msg,['ERROR: rg{' int2str(isrg) '}: at least for one factor, sum of cr not egal to 1']);
            end
            is_pb=false;
            for v=1:nv; if sum(srg.cr(srg.Vr(:,v)))~=1; is_pb=true; end; end
            if is_pb
                msg = char(msg,['ERROR: rg{' int2str(isrg) '}: at least for one variable, sum of cr not egal to 1']);
            end
        end
    end

if ~isempty(msg); is_OK = false; msg(1,:)=[]; end
end
