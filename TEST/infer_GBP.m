function [ marginals,beliefs,mess,delta, Stop_Oscil ] = gm_infer_GBP( FG,RG,precision,max_iter,damp,verbose )
% This function computes marginals, beliefs and messages, for a given
% graphical model, using generalized belief propagation.
% INPUT:
% - fg: a factor graph structure where :
%   - fg.doms: vector 1 x n of variables domains sizes.
%   - fg.G: matrix m x n, where m is the number of factors and n the number of variables 
%   - fg.factors: a structure of m factors, in the form of multidimensional
%   arrays. 
%  - rg: A region graph.
%    - rg.regions: matrix q x (n+m). The full set of q regions of the CVM
%    region graph.
%    - rg.G: matrix q x q. The region graph edges.
%    - rg.c: vector 1 x q. The count vector of the regions.
%  - precision: a precision required between successive marginals.
%  - max_iter: A maximum number of iterations of GBP.
%  - damp: damping factor in messages updates
% OUTPUT:
% - beliefs: a list of r "beliefs".
%   beliefs{r} is an array (which elements sum to one) composing the belief
%   function for each of the q regions.
% - marginals: a list of n vectors, composing the marginal probabilities of
%   every variables, as computed by marginalisation of beliefs.
%  - mess: a list of r messages.
%    mess{k} is an array, which dimensions are the sizes of the
%    domains of the target region.
%  - delta: a matrix of all successive marginals maximum differences...
%  - Stop_Oscil: Boolean. Is true if and only if the loop is stopped for
%  reason of equal successive maximal difference in marginals.


% CONVERT attributs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fg.doms=FG.Card;
fg.G=FG.BG;
for i=1:length(FG.F)
    if isa(FG.F{i}.T, 'function_handle'); fg.factors{i}=FG.F{i}.T();
    else fg.factors{i}=FG.F{i}.T; end
end
rg.regions = [RG.Vr RG.Fr];
rg.G = RG.Gr;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin<6
    verbose=0; % No intermediate printing.
end
if nargin<5
    damp=0; % No damping.
end
if nargin<4
    max_iter=100; % 100 iterations max by default.
end
if nargin<3
    precision=0.0001; % Default precision: 0.0001.
end

% Compute the elements which will be required in the parent-to-child GBP
% algorithm:
[ Desc,Eps,N,D ] = Elements_RG( rg.G );

% Compute the order of messages in the parent-to-child GBP algorithm:
[ Order ] = Compute_mess_order( rg.G );

% Initialize messages, beliefs and marginals.
[ mess ] = Update_messages_P_to_C(fg,rg,Order,N,D,damp);
mess = Normalize_mess(mess);
[ beliefs, marginals ] = Compute_beliefs_2(fg,rg,Order,Desc,Eps,mess);

t=0; % Iteration.

% If we would like to print current marginals
if verbose==1 % Let us print the marginals
    for j=1:numel(marginals)
        for i=1:numel(marginals{j})
            MARG(i,j) = marginals{j}(i);
        end
    end
    MARG
end

delta_loc=1; % Initial "error" 
Stop_Oscil = false; % Oscillations detection.
% Main loop:
while (t<max_iter)&&(delta_loc>precision)&&(~Stop_Oscil)
    t = t+1;
    mess_old = mess;
    
    [ mess ] = Update_messages_P_to_C(fg,rg,Order,N,D,damp,mess);
    [ mess ] = Normalize_mess(mess);
    [ beliefs, marginals ] = Compute_beliefs_2( fg,rg,Order,Desc,Eps,mess);
    
    % If we would like to print current marginals
    if verbose==1 % Let us print the marginals
        for j=1:numel(marginals)
            for i=1:numel(marginals{j})
                MARG(i,j) = marginals{j}(i);
            end
        end
        MARG
    end
    
    % Compute delta:
    if ~isempty(mess)
        for i=1:length(mess)
            delta_i = max(abs(reshape(mess{i}-mess_old{i},numel(mess{i}),1)));
            delta(t,i) = delta_i;
        end
        delta_loc = max(delta(t,:));
    else
        delta = 0;
        delta_loc = 0;
    end
    
    % Detect oscillations:
    if t>1
        Q=delta-repmat(delta(size(delta,1),:),size(delta,1),1);
        Oscil=max(abs(Q),[],2);
        if min(Oscil(1:(t-1)))<0.000001 % Looks like an oscillation...
            Stop_Oscil=true;
        end
    end
        
end
end


function [ Desc,Eps,N,D ] = Elements_RG( G )
% This functions computes subsets of vertices and edges of a region graph,
% which are necessary in the GBP parent to child algorithm.
% INPUT:
%   - G: A matrix qxq of edges of a region graph.
% OUTPUT:
%   - Desc: A qxq matrix. Desc(i,j)=1 if j is a descendants of i in G.
%   - Eps: A qxq matrix. Eps(i,j)=1 if either j=i or j is a descendant of i
%   in G.
%   - N: A qxq array. For any edge (p,r) of G, N{p,r} is a matrix M qxq,
%   such that M(i,j)=1 iff 
%                         - Eps(p,j)=1,
%                         - Eps(r,j)=0,
%                         - Eps(p,i)=0.
%     If (p,r) is not an edge of G, N{p,r}=[];
%   - D: A qxq array. For any edge (p,r) of G, D{p,r} is a matrix M qxq,
%   such that M(i,j)=1 iff 
%                         - Eps(r,j)=1,
%                         - Eps(r,i)=0,
%                         - Eps(p,i)=1.
%     If (p,r) is not an edge of G, D{p,r}=[];

%%%% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! %%%%%%%%%
% The definition of D(P,R) in YFW seems wrong!
% Let us try : 
%   - D: A qxq array. For any edge (p,r) of G, D{p,r} is a matrix M qxq,
%   such that M(i,j)=1 iff 
%                         - Eps(r,j)=1,
%                         - Eps(r,i)=0,
%                         - Desc(p,i)=1. !!!!!! AND NOT EPS!!!
%     If (p,r) is not an edge of G, D{p,r}=[];

q=size(G,1);

% Computation of the descendants matrix.
Desc=G;
t=2;
H=G^t;
while ~isempty(find(H))
    Desc=((Desc+H)>0); % Add descendants at t.
    t=t+1;
    H=G^t;
end

% Add the nodes themselves to descendants.
Eps=Desc+eye(q);

% Compute edges of G:
[P R]=find(G);

% Compute cell arrays N and D.
N=cell(q,q);
D=cell(q,q);
for k=1:length(P)
    M=zeros(q,q);
    MM=zeros(q,q);
    p=P(k);
    r=R(k);
    for i=1:q
        for j=1:q
            M(i,j)=Eps(p,j)*(1-Eps(r,j))*(1-Eps(p,i));
            MM(i,j)=Eps(r,j)*(1-Eps(r,i))*Desc(p,i);
        end
    end
    N{p,r}=M.*G;
    D{p,r}=MM.*G;
    if all(all(N{p,r}==0)) % N{p,R} is empty!
        N{p,r}=[];
    end
    if all(all(D{p,r}==0)) % D{p,R} is empty!
        D{p,r}=[];
    end
end   
end


function [ Order ] = Compute_mess_order( G )
% This function computes the order of messages to be sent in GBP, for a
% region graph G.
% INPUT : G: matrix q x q. The region graph edges.
% OUTPUT : Order: a r x 2 matrix, where r is the number of edges.
% Order(k,:) contains the start and end nodes numbers of the kth edge,
% corresponding to the kth message that will be updated during one iteration of the GBP loop.

% Do not take parents that do not have all children considered - MJO 09/03/18 

% q=size(G,1);
% Order=zeros(nnz(G),2);
% k=1;
% while sum(sum(G))>0 % There are still edges remaining
%     % Find the indices of the leaves of current graph G:
%     %I=(find((sum(G,2)==0).*(sum(G,1)>0)');
%     I=intersect(find(sum(G,2)==0),find(sum(G,1)>0));
%     % Add them to the order
%     for j=I'
%         for i=find(G(:,j))' % edge (i,j) is added to order and substracted from G
%             Order(k,:)=[i j];
%             k=k+1;
%             %G(i,j)=0;
%         end
%         G(:,j)=0;
%     end
% end

Order=zeros(nnz(G),2);
% Consider first messages to leaf regions, last messages go to root regions
m_order=[]; Gr=G;
leaf = find(sum(Gr,2)'==0 & sum(Gr,1)>0);
Rremain_bool = true(1,size(G,1));
while ~isempty(leaf)
    Rremain_bool(leaf)=false; 
    new_m=[];
    for m_end=leaf
        m_beg=find(Gr(:,m_end))';
        % do not take parents (m_beg) that have children not taking into account
        for beg=m_beg
            children_bool = Gr(beg,:);
            if ~any( Rremain_bool(children_bool) ); new_m=[new_m [beg; m_end]]; end
        end
    end
    for i=1:size(new_m,2);  Gr(new_m(1,i), new_m(2,i))=false; end
    m_order = [m_order new_m];
    leaf = find(sum(Gr,2)'==0 & sum(Gr,1)>0);
    
    Order=m_order';
end
% end MJO 09/03/18 
end

% Add damp arg to be always given - MJO 16/03/18
function [ mess_updated ] = Update_messages_P_to_C(fg,rg,Order,N,D,damp,mess_old )
% This function updates messages in the Parent-to-child algorithm.
% INPUT: 
% - fg: a factor graph structure where :
%   - fg.doms: vector 1 x n of variables domains sizes.
%   - fg.G: matrix m x n, where m is the number of factors and n the number of variables 
%   - fg.factors: a structure of m factors, in the form of multidimensional
%   arrays. 
%  - rg: A region graph.
%    - rg.regions: matrix q x (n+m). The full set of q regions of the CVM
%    region graph.
%    - rg.G: matrix q x q. The region graph edges.
%    - rg.c: vector 1 x q. The count vector of the regions.
%  - Order: a r x 2 matrix, where r is the number of edges.
% Order(k,:) contains the start and end nodes numbers of the kth edge,
% corresponding to the kth message that will be updated during one iteration of the GBP loop.
%   - N: A qxq array. For any edge (p,r) of G, N{p,r} is a matrix M qxq,
%   such that M(i,j)=1 iff 
%                         - Eps(p,j)=1,
%                         - Eps(r,j)=0,
%                         - Eps(p,i)=0.
%     If (p,r) is not an edge of G, N{p,r}=[];
%   - D: A qxq array. For any edge (p,r) of G, D{p,r} is a matrix M qxq,
%   such that M(i,j)=1 iff 
%                         - Eps(r,j)=1,
%                         - Eps(r,i)=0,
%                         - Eps(p,i)=1.
%     If (p,r) is not an edge of G, D{p,r}=[];
%  - mess_old: a list of r messages.
%    mess_old{k} is an array, which dimensions are the sizes of the
%    domains of the target region Order(k,2).
%  - damp: A real between 0 (no damping) to 1 (no update). A value <1 may
%  help GBP not to oscillate (Murphy, Weiss, Jordan, 1999).
% OUTPUT:
%  - mess_updated: a list of r messages.
%    mess_updated{k} is an array, which dimensions are the sizes of the
%    domains of the target region Order(k,2).
n=numel(fg.doms);
m=size(fg.G,1);

%% We consider the case where rg is made of a single region (naive exact calculus)
if isempty(Order) % No messages!
    mess_updated=[];
else % there is at least one message:    
% Initialize mess_old, if needed (first iteration).
if nargin<7 % Add damp arg to be always given - MJO 16/03/18
    for k=1:size(Order,1) % Loop over all messages
        r=Order(k,2); % target region, defining the length of the kth message.
        Ir=find(rg.regions(r,1:n)); % The indices of the variables in region r.
        if numel(Ir)==1 % Particular case of single dimension
            mess_old{k}=ones(fg.doms(Ir),1);
        else
            mess_old{k}=ones(fg.doms(Ir));
        end
    end
end
% Add damp arg to be always given - MJO 16/03/18
% if nargin<7
%     damp = 0;
% end

% Define the updated messages, for the ones that will not be modified in
% the current loop.
%mess_updated=mess_old;

% Loop over all messages to update
for k=1:size(Order,1)
    %mess_updated{k}=mess_old{k};
    
    % Regions concerned:
    p=Order(k,1);
    r=Order(k,2);
    
    % We update message m_{p->r}
    I_p=find(rg.regions(p,1:n)); % variables in region p.
    I_r=find(rg.regions(r,1:n)); % variables in region r.
    F_pr=setdiff(find(rg.regions(p,(n+1):(n+m))),find(rg.regions(r,(n+1):(n+m)))); % factors in F_P\R
    
    % Compute the "extended" numerator (i.e. over all variables in region p).
    %Numerator=ones(fg.doms(I_p)); % Initialisation as a multidimensional array over variables in p.
    
    % Multiply all the factors:
    if ~isempty(F_pr)
        factor_list={};
        scopes_list={};
        for f=F_pr % for all factors
            factor_list{length(factor_list)+1} = fg.factors{f}; % factor f
            scopes_list{length(scopes_list)+1} = find(fg.G(f,:)); % Variables in factor f
        end
        %factor_list
        %scopes_list
        [Numerator_factor,scope_factors] = Prod_subfunctions(factor_list,scopes_list);
    else % No factors
        Numerator_factor = 1;
        scope_factors = [];
    end
    
    % Multiply all the messages in N(p,r):
    if ~isempty(N{p,r})
        [I,J]=find(N{p,r}); % all edges in N(p,r).
        messages_list={};
        scopes_mess_list={};
        for x=1:length(I) % for all messages m_{I(x)->J(x)}
            [~,edge_number]=ismember([I(x) J(x)],Order,'rows'); % index of edge (I(x)->J(x) in rg.G.
            messages_list{length(messages_list)+1} = mess_old{edge_number}; % Message x.
            scopes_mess_list{length(scopes_mess_list)+1} = find(rg.regions(J(x),1:n)); % Scope of message x
        end
        [Numerator_mess,scope_mess] = Prod_subfunctions(messages_list,scopes_mess_list);
    else
        scope_mess = I_r;
        if length(I_r)>1 % More than one variable in r
            Numerator_mess = ones(fg.doms(I_r)); % A message mp-r with dimensions that of I_r, and ones everywhere
        else % Be careful that Numerator_mess should be of size [x 1] and not [x x]
            Numerator_mess = ones(fg.doms(I_r),1);
        end
    end
    
    % Now, multiply Numerator_factor and Numerator_mess to get the
    % numerator...
    [Numerator,scope_Numerator] = Prod_subfunctions({Numerator_factor,Numerator_mess},{scope_factors,scope_mess});                    
    
    % Marginalize the numerator over the variables in P\R
    scope_out = setdiff(I_p,I_r); % Indices over which we marginalize Numerator
    
    [Numerator,scope_Numerator] = Marginalize_out( Numerator,scope_Numerator,scope_out);
    
    % Compute the denominator...
    
    % Multiply all the messages in D(p,r):
    if ~isempty(D{p,r})
        [I,J]=find(D{p,r}); % all edges in D(p,r).
        messages_list={};
        scopes_mess_list={};
        for x=1:length(I) % for all message m_{I(x)->J(x)}
            [~,edge_number]=ismember([I(x) J(x)],Order,'rows'); % index of edge (I(x)->J(x) in rg.G.
            messages_list{length(messages_list)+1} = mess_updated{edge_number}; % Message edge number.
            scopes_mess_list{length(scopes_mess_list)+1} = find(rg.regions(Order(edge_number,2),1:n)); % Scope of message edge number
        end
        
        [Denominator,scope_Denominator] = Prod_subfunctions(messages_list,scopes_mess_list);
    else
       % messages of scope I_r and equal to 1 everywhere
        scope_Denominator = I_r;
        if length(I_r)>1 % More than one variable in r
            Denominator = ones(fg.doms(I_r)); % A message mp-r with dimensions that of I_r, and ones everywhere
        else % Be careful that Numerator_mess should be of size [x 1] and not [x x]
            Denominator = ones(fg.doms(I_r),1);
        end
    end
        
    % Message update
    
    Inv_Denominator = 1 ./Denominator; % We take the inverse of the Denominator, before taking the product.
    
    %  sometimes not all region variables are in new message - MJO 16/03/18
    [mess_new, V_out] = Prod_subfunctions({Numerator,Inv_Denominator},{scope_Numerator,scope_Denominator});
    
    V_not_present = rg.regions(r,1:n); V_not_present(V_out)=false;
    if any(V_not_present)
        if sum(rg.regions(r,1:n))==1; M=zeros(fg.doms(rg.regions(r,1:n)),1);
        else M=zeros(fg.doms(rg.regions(r,1:n))); end
        card = fg.doms(V_not_present);
        for i=1:prod(card)
            i_v = itov(i,card); n_i_v=1; cmd='M(';
            for v=find(rg.regions(r,1:n))
                if V_not_present(v); cmd=[cmd int2str(i_v(n_i_v)) ',']; n_i_v= n_i_v +1;
                else cmd=[cmd ':,']; end
            end
            cmd=cmd(1:(length(cmd)-1)); cmd=[cmd ') = mess_new;']; eval(cmd);
        end
        mess_new=M;
    end
    % end MJO 16/03/18
    
    mess_updated{k} = (1-damp)*mess_new+damp*mess_old{k};
end
end
end

function v = itov (i, Y)
% Tranform the integer i in a vector of integers
% following the possibility of variation defined by vector Y
% Example: itov(11, [3 2 2]) = [3 2 1]
%          itov(10, [2 3 2]) = [2 2 2]

i = i-1;
W = [fliplr( cumprod( fliplr (Y(2:length(Y))) ) ) 1];

v = zeros(1,length(W));
for t = 1:length(W)
    x = floor(i/ W(t));
    v(t) = x+1;
    i = i - x*W(t);
end
end



function [ factor_marg, scope_marg ] = Marginalize_out( factor,scope,scope_out )
% This function takes factor {factor,scope} as input, as well as the
% variables which should be marginalized out (scope_out) and returns a
% marginalized factor, {factor_marg, scope_marg}
% INPUT:
%    - factor : a m multidimensional array (of dimension length(scope)).
%    - scope : A vectors containing the indices of the variables of factor.
%    - scope_out : A vectors containing the indices of the variables that
%    we want to marginalize out in factor.
% OUTPUT:
%    - factor_marg: a multidimensional array representing the marginalized factor. 
%    - scope_out: the scope of factor_marg.

% Compute the scope scope_out of factor_marg.
scope_marg = setdiff(scope,scope_out); % the scope of the marginalized factor contains the variables of factor, which are not marginalized out.

% Compute the marginalized factor:
[~,I_marg_out_in_factor] = ismember(scope_out,scope); % I_marg_out_in_factor contains the indices of the indices of variables of factor, which will be marginalized out 
                                                      % (i.e. scope_out = setdiff(scope,scope(I_marg_out_in_factor))).
I_marg_out_in_factor(I_marg_out_in_factor==0)=[]; % Using ismember, I_marg_out_in_factor contains 0 whenever variables in scope_out are not variables in scope... Instead, we should ignore them...

factor_marg = factor; % Initial value of the marginalized factor.
if ~isempty(I_marg_out_in_factor) % Else, we have nothing to marginalize out...                                                 
    I_marg_out_in_factor = sort(I_marg_out_in_factor,'descend'); % We sort the variables to marginalize out in decreasing order, in order not to have a dimension problem when we sum...
    for i = I_marg_out_in_factor
        factor_marg = squeeze(sum(factor_marg,i));
    end
end

if size(factor_marg,1)==1 % factor_marg is a line vector...
    factor_marg = factor_marg'; % We should make it a column vector...
end

end





function [ f_out,scope_out ] = Prod_subfunctions( f_list,scopes_list )
% This function computes the products of subfunctions in f_list, of limited
% scopes in scopes_list.
% INPUT:
%    - f_list : a list of m multidimensional arrays : f_list{k} is a
%    multidimensional array of dimension length(scopes_list{k}).
%    - scopes_list : A list of vectors of indices of variables.
%    scopes_list{k} is the list of vectors of variables in f_list{k}.
% OUTPUT:
%    - f_out: a multidimensional array. 
%    - scope_out: the scope of f_out. It is the union of the scopes in scopes_list.

% Remove the empty factors i.e. the constants
for k=length(scopes_list):-1:1
    if isempty(scopes_list{k}) % It should be removed!
        scopes_list(k) = []; % Removes the k-th element and does not replace it with []!
        f_list(k) = []; % the same.
    end
end

m = length(scopes_list); % The number of subfunctions.

% Computation of scope_out:
% Initialization    
if (m==0)
    scope_out=[]; % Empty scope.
    f_out=1;
elseif (m==1) % Single function in, single function out...
    f_out=f_list{1};
    scope_out=scopes_list{1};
else % At least two factors
    
    % We compute scope_out.
    scope_out = [];
    for k=1:m
        scope_out=union(scope_out,scopes_list{k});
    end    
    scope_out=reshape(scope_out,1,numel(scope_out));

    %scope_out=scope_out';
    % We have to check the domains of each subfunction and see whether they are
    % compatible.
    Domains = zeros(m,numel(scope_out)); % Domains is a matrix for which each line will represent the domain sizes of the variables in the subfactor.
    for k=1:m % For each subfunction
        [~,Sk_in_Sout] = ismember(scopes_list{k},scope_out); % Indices of the variables in function k in scope_out.
        size_fk=size(f_list{k});
        if numel(Sk_in_Sout)>1 % at least two dimensions in f_list{k}
            Domains(k,Sk_in_Sout) = size_fk;
        elseif numel(Sk_in_Sout)==1 % Single dimension
            Domains(k,Sk_in_Sout) = prod(size_fk);
        end     
    end
   
    % In each column, the non-zero elements should all be identical!
    error=0;
    for i=1:size(Domains,2)
        col_i=Domains(:,i);
        if numel(unique(col_i(find(col_i))))>1 % there are at least two non-equal non-zero elements.
            error=1;
        end
    end
    %Domains
    %max(Domains,[],1)

    if error==1
        X = 'Error 1: Incompatible domains'
        f_out=[];
    else
        % Now, we can compute f_out, using function Redim and operator .*
        if size(Domains,2)>1 % In this case, everything is fine.
            f_out = ones(max(Domains,[],1));
        else % We must take care of the case where there is a single variable in Domains : ones(d) will not do!
            f_out = ones(max(Domains,[],1),1); % In this case, f_out is a vector!
        end
        for k=1:m
            f_out = f_out .* Redim(f_list{k},scopes_list{k},scope_out,max(Domains,[],1));
        end
    end
end
end


function [ f_out ] = Redim( f,A,B,dims )
% This function extends the scope of f from A to a superset B of A, while
% leaving the values unchanged (new dimensions have singleton domains.
% INPUT:
%   - f: A multidimensional array, which variables are indexed in A.
%   - A: Vector of variables of f indices.
%   - B: A superset of A.
%   - dims is a vector of length numel(B). its elements are the sizes of
%   the domains and f_out, which should be compatible with f...
% OUTPUT:
%   - f_out: A multidimensional array, which variables are indexed in B.
%   all dimensions not in A are that of dims, so that the elements of f_out are exactly those of f replicated.

if ~isempty(setdiff(A,B)) % Houston, we have a problem: A is not a subset of B!
    f_out=[];
    pb='problem 1'
elseif isempty(A) % Coquinou, f=[]!
    f_out = [];
else % Relieved!
    % Let us compute the sizes 
    %size_f = size(f); % Domains of variables of f.
    s_out = ones(1,numel(B)); 
    [~,A_in_B] = ismember(A,B); % A_in_B contains the indices of elements of A in B.
    if numel(A_in_B)>1 % That is f is a function of more than one variable
        s_out(A_in_B) = size(f);
    else % That is f is a function of a single variable, but still, size(f) has two elements!!
        s_out(A_in_B) = prod(size(f)); % Yes, one of the two elements of size(f) is 1 ;-)
    end
    % In this way, s_out represent the domains of f_out, with singleton
    % dimensions for variables which are not in f...
    
    % Now lets fill in the elements of f in f_out :
    if numel(s_out)>1 % All is fine when there are at least 2 elements in B
        f_out = reshape(reshape(f,prod(size(f),1)),s_out); % Here is the miracle...
    else
        f_out = reshape(reshape(f,prod(size(f),1)),s_out,1); % This to get a vector when s_out is a single number.
    end
    
    % OK, we are left with inflating the dimensions to stick to dims...
    % But first check that dims is compatible with f!
    if (numel(A_in_B)>1)&&~isempty(find(dims(A_in_B)-size(f))) % Incompatibility in dimensions of f
        f_out=[];
        pb='problem 2'
    %elseif (numel(A_in_B)==1)&&(numel(A_in_B)~=prod(size(f))) % Still have to consider the case of 1D factor separately )-;
    else % OK, then repmat!
        inflate = dims;
        inflate(A_in_B) = 1;
        f_out = repmat(f_out,inflate);
    end
        
end
end


function [ messages_normalized ] = Normalize_mess( messages )
% This function normalizes the messages. 
% It is known (e.g. thesis Victor Martin, 2013) that convergence of BP with
% normalized messages is not affected. Should be the same for GBP...
% INPUT:
%  - messages: a list of r messages.
%    messages{k} is an array, which dimensions are the sizes of the
%    domains of the target region Order(k,2).
% OUTPUT:
%  - messages_normalized: Normalized messages.
% REMARK: Several normalizations are possible ~m_I->J = 1/c_ij m_I->J with
% any positive constants c_ij is fine. We take c_ij =max_x_J(m_I->J(x_J)) 
% but the sum can be taken as well. With our choice, all messages maximum
% is 1 (except when one message is uniformly 0, which is bad news...).

%% Consider the case where there are no messages!
if isempty(messages)
    messages_normalized=[];
else
epsilon=0.0001;
for k=1:length(messages)
    c_k = max(reshape(messages{k},prod(size(messages{k})),1)); % Normalization factor.
    if c_k>0 % case when the message is not uniformly 0:
        messages_normalized{k} = messages{k}/c_k;
    else % We don't change current message:
        messages_normalized{k} = messages{k};
    end
    
    % This is dirty... But it avoids having null messages which, in turn,
    % may lead to infinite messages (by division) which can not be
    % normalized!
    messages_normalized{k} = max(messages_normalized{k},epsilon);
end
end
end


function [ beliefs, marginals ] = Compute_beliefs_2( fg,rg,Order,Desc,Eps,mess )
% This function computes beliefs from messages, using Eq (113) of YFW.
% INPUT: 
% - fg: a factor graph structure where :
%   - fg.doms: vector 1 x n of variables domains sizes.
%   - fg.G: matrix m x n, where m is the number of factors and n the number of variables 
%   - fg.factors: a structure of m factors, in the form of multidimensional
%   arrays. 
%  - rg: A region graph.
%    - rg.regions: matrix q x (n+m). The full set of q regions of the CVM
%    region graph.
%    - rg.G: matrix q x q. The region graph edges.
%    - rg.c: vector 1 x q. The count vector of the regions.
%  - Order: a r x 2 matrix, where r is the number of edges.
% Order(k,:) contains the start and end nodes numbers of the kth edge,
% corresponding to the kth message that will be updated during one iteration of the GBP loop.
%   - Desc: A qxq matrix. Desc(i,j)=1 if j is a descendants of i in G.
%   - Eps: A qxq matrix. Eps(i,j)=1 if either j=i or j is a descendant of i
%   in G.
%  - mess: a list of r messages.
%    mess{k} is an array, which dimensions are the sizes of the
%    domains of the target region Order(k,2).
% OUTPUT:
% - beliefs: a list of r "beliefs".
%   beliefs{r} is an array (which elements sum to one) composing the belief
%   function for each of the q regions.
% - marginals: a list of n vectors, composing the marginal probabilities of
%   every variables, as computed by marginalisation of beliefs.

q = size(rg.regions,1); % Number of regions.
n = length(fg.doms); % Number of variables.
m = size(fg.G,1); % Number of factors.

%% Loop over the q regions:
for r=1:q
    % Compute the list of factors in region r:
    F_r = find(rg.regions(r,(n+1):(n+m))); % Indices of the factors in region r.
    
    %% Compute the list of messages in region r:
    % First, the messages from parents, to r:
    M_r = find(Order(:,2)==r)';
    
    % Second, the messages to descendants of r, from other regions than r
    % which are not descendants of r.
    for d=find(Desc(r,:)) % Descendants of r.
        for p=find(rg.G(:,d))' % Parents of d.
            if Eps(r,p)==0 % p is neither r nor one of its descendants.
                [~,k] = ismember([p d],Order,'rows'); % one more message to add.
                M_r=[M_r k];
            end
        end
    end
        
    % Multiply all the factors and messages:
    factor_list={};
    scopes_list={};
    if ~isempty(F_r) % There is at least one factor 
        for f=F_r % store all factors in a list
            factor_list{length(factor_list)+1} = fg.factors{f}; % factor f
            scopes_list{length(scopes_list)+1} = find(fg.G(f,:)); % Variables in factor f
        end
    end
    if ~isempty(M_r) % There is at least one message 
        for k=M_r % Add the messages
            factor_list{length(factor_list)+1} = mess{k}; % message k
            scopes_list{length(scopes_list)+1} = find(rg.regions(Order(k,2),1:n)); % variables in target region of message k.
        end
    end
    
    %%% Je ne sais pas si on peut passer dans le if qui suit!
    if isempty(F_r)&&isempty(M_r) % Nothing to multiply!
        Ir = find(rg.regions(r,1:n)); % The indices of the variables in region r.
        beliefs{r} = ones(fg.doms(Ir)); % Initialization of the beliefs.
    else % We can multiply...
        [beliefs{r},scope_Prod_factor] = Prod_subfunctions(factor_list,scopes_list);
    end
    
    % Normalize belief{r} 
    beliefs{r} = beliefs{r}/sum(reshape(beliefs{r},prod(size(beliefs{r})),1),1);    
end

% Now compute marginals{i}
for i=1:n
    % Find minimal size region ri containing variable i:

    % To compare coding : choose the downest region - MJO 09/02/18        
%     Ri = find(rg.regions(:,i)); % Ri is the set of regions containing i.
%     sRi = sum(rg.regions(Ri,1:n),2); % sRi is the vector of sizes of regions in Ri.
%     [~,xi] = min(sRi);
%     ri = Ri(xi); % A minimal size region containing i.
    Vr = rg.regions(:,1:n);
    R = find(Vr(:,i)); R=R(end); ri=R;
    % end MJO 09/02/18  

    % In order to compute marginals{i}, we permute the dimensions of
    % beliefs{ri} so that i is the first variable, then we reshape the
    % obtained array to get a matrix of size doms(i) x product of other
    % domains sizes), and then we sum this matrix over dimension 2.
    
    [~,i_in_ri] = ismember(i,find(rg.regions(ri,1:n))); % Place of i in ri.
    marginals{i} = marginalize(beliefs{ri},i_in_ri);
end
end    


function [ Pi ] = marginalize( f,i )
% This function marginalizes out all variables of f except variable i.
% INPUT: 
%       - f: a n-dimensional array
%       - i the index of the variable of f on which the marginal is
%       computed.
% OUTPUT:
%       - Pi: a size(f,i) x 1 vector containing the marginalization over i
%       of f.

if isvector(f) % f is a vector!
    Pi = f; % No need to marginalize
elseif i>ndims(f) % i is not a variable of f!
    Error = 'i exceeds the dimension of f!'
    Pi=[];
else % f is at least a 2-D array and i is the index of one of its variables.
    Pi = permute(f,[i setdiff(1:ndims(f),i)]); % the dimensions of f as permuted so that dim i is first.
    Pi = reshape(Pi,size(Pi,1),prod(size(Pi))/size(Pi,1)); % Pi is reshaped as a matrix.
    Pi = sum(Pi,2); % Here is the marginal!
end
end
