% Test gm_rg_CVM.m

disp('***** Test gm_rg_CVM.m *****')
clear fg rg rg_expected;

fg = example_Tiny();
rg = gm_rg_CVM(fg);
%rg_expected=rg; save('EXPECTED/rg_CVM1.mat','rg_expected');
load('EXPECTED/rg_CVM1.mat')
if compareRG(rg, rg_expected); disp('Test Tiny: OK'); 
else disp('Test Tiny: PB'); is_OK=false; end
clear fg rg rg_expected;


[fg, regions] = example_Trivial();
rg = gm_rg_CVM(fg); 
%rg_expected=rg; save('EXPECTED/rg_CVM2.mat','rg_expected');
load('EXPECTED/rg_CVM2.mat')
if compareRG(rg, rg_expected); disp('Test Trivial 1: OK'); 
else; disp('Test Trivial 1: PB'); is_OK=false; end
rg = gm_rg_CVM(fg,regions); 
%rg_expected=rg; save('EXPECTED/rg_CVM3.mat','rg_expected');
load('EXPECTED/rg_CVM3.mat')
if compareRG(rg, rg_expected); disp('Test Trivial 2: OK'); 
else; disp('Test Trivial 2: PB'); is_OK=false; end
clear fg rg rg_expected;


[fg, regions, regions_jt1] = example_Lattice();
rg = gm_rg_CVM(fg);
%rg_expected=rg; save('EXPECTED/rg_CVM4.mat','rg_expected');
load('EXPECTED/rg_CVM4.mat')
if compareRG(rg, rg_expected); disp('Test Lattice 1: OK'); 
else; disp('Test Lattice 1: PB'); is_OK=false; end
rg = gm_rg_CVM(fg, regions); 
%rg_expected=rg; save('EXPECTED/rg_CVM5.mat','rg_expected');
load('EXPECTED/rg_CVM5.mat')
if compareRG(rg, rg_expected); disp('Test Lattice 2: OK'); 
else; disp('Test Lattice 2: PB'); is_OK=false; end
rg = gm_rg_CVM(fg, regions_jt1); 
%rg_expected=rg; save('EXPECTED/rg_CVM6.mat','rg_expected');
load('EXPECTED/rg_CVM6.mat')
if compareRG(rg, rg_expected); disp('Test Lattice 3: OK'); 
else; disp('Test Lattice 3: PB'); is_OK=false; end
clear fg rg rg_expected regions regions_jt1;


fg = example_YedidiaFig4();
rg = gm_rg_CVM(fg); 
%rg_expected=rg; save('EXPECTED/rg_CVM7.mat','rg_expected');
load('EXPECTED/rg_CVM7.mat')
if compareRG(rg, rg_expected); disp('Test Figure4 1: OK'); 
else; disp('Test Figure4: PB'); is_OK=false; end
clear fg rg rg_expected ;


global NS; NS=3; fg = gm_example_DBN(5,5);
rg = gm_rg_CVM(fg);
%rg_expected=rg; save('EXPECTED/rg_CVM8.mat','rg_expected');
load('EXPECTED/rg_CVM8.mat')
if compareRG(rg, rg_expected); disp('Test DBN 5-3-5 1: OK'); 
else; disp('Test DBN 5-3-5 1: PB'); is_OK=false; end
clear fg rg rg_expected NS;


fg= gm_read_fg('FG/example_Alarm_pb.uai');
rg = gm_rg_CVM(fg);
%rg_expected=rg; save('EXPECTED/rg_CVM9.mat','rg_expected');
load('EXPECTED/rg_CVM9.mat')
if compareRG(rg, rg_expected); disp('Test Alarm: OK'); 
else; disp('Test Alarm: PB'); is_OK=false; end
clear fg rg rg_expected ;


