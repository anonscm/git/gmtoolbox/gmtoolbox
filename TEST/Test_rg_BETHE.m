% Test gm_rg_BETHE.m

disp('***** Test gm_rg_BETHE.m *****')
clear fg rg rg_expected;

fg = example_Tiny();
rg = gm_rg_BETHE(fg); 
%rg_expected=rg; save('EXPECTED/rg_BETHE1.mat','rg_expected');
load('EXPECTED/rg_BETHE1.mat')
if compareRG(rg, rg_expected); disp('Test Tiny: OK'); 
else disp('Test Tiny: PB'); is_OK=false; end
clear fg rg rg_expected;


fg = example_Trivial();
rg = gm_rg_BETHE(fg); 
%rg_expected=rg; save('EXPECTED/rg_BETHE2.mat','rg_expected');
load('EXPECTED/rg_BETHE2.mat')
if compareRG(rg, rg_expected); disp('Test Trivial 1: OK'); 
else; disp('Test Trivial 1: PB'); is_OK=false; end
clear fg rg rg_expected;


fg = example_Lattice();
rg = gm_rg_BETHE(fg); 
%rg_expected=rg; save('EXPECTED/rg_BETHE3.mat','rg_expected');
load('EXPECTED/rg_BETHE3.mat')
if compareRG(rg, rg_expected); disp('Test Lattice 1: OK'); 
else; disp('Test Lattice 1: PB'); is_OK=false; end
clear fg rg rg_expected;


fg = example_YedidiaFig4();
rg = gm_rg_BETHE(fg);
%rg_expected=rg; save('EXPECTED/rg_BETHE4.mat','rg_expected');
load('EXPECTED/rg_BETHE4.mat')
if compareRG(rg, rg_expected); disp('Test Figure4 1: OK'); 
else; disp('Test Figure4 1: PB'); is_OK=false; end
clear fg rg rg_expected NS;


global NS; NS=3; fg = gm_example_DBN(5,5);
rg = gm_rg_BETHE(fg); 
%rg_expected=rg; save('EXPECTED/rg_BETHE5.mat','rg_expected');
load('EXPECTED/rg_BETHE5.mat')
if compareRG(rg, rg_expected); disp('Test DBN 5-3-5 1: OK'); 
else; disp('Test DBN 5-3-5 1: PB'); is_OK=false; end
clear fg rg rg_expected NS;



