function [fg, rg] = example_YedidiaFig11()

% Define a factor grap combatible with region graph of Figure 11 (Yedidia 2014)
nv=5; 
nf=5;

fg.Card = 2*(1:nv); 

sfg.Vcode=1:nv;
sfg.F{1}.V=1; 
sfg.F{2}.V=[1 2]; 
sfg.F{3}.V=[2 3]; 
sfg.F{4}.V=[1 4]; 
sfg.F{5}.V=[1 2 5]; 
sfg.F{1}.T=[0.5; 0.5];
for i=2:nv
    rng(i); 
    T=rand(fg.Card(sfg.F{i}.V)); 
    T=T/sum(reshape(T,1,numel(T))); 
    sfg.F{i}.T=T;
end
sfg.BG = setBG( nv, sfg.F);

fg.sfg{1}=sfg;


% Describe the region graph
nr=9;
srg.Vr=false(nr,nv);
srg.Vr(1,[1 2 5])=true; srg.Vr(2,[1 2 4])=true; 
srg.Vr(3,[1 2])=true; srg.Vr(4,[1 2 4])=true;
srg.Vr(5,1)=true; srg.Vr(6,[1 2])=true; srg.Vr(7,[2 3])=true;
srg.Vr(8,1)=true;srg.Vr(9,2)=true;

srg.Fr=false(nr,nf);
srg.Fr(1,[1 2 5])=true; srg.Fr(2,[1 2 4])=true;
srg.Fr(3,[1 2])=true; srg.Fr(4,[2 4])=true;
srg.Fr(5,1)=true; srg.Fr(6,2)=true; srg.Fr(7,3)=true;

srg.Gr=false(nr);
srg.Gr(1,3)=true; srg.Gr(2,[3 4])=true; 
srg.Gr(3,[5 6])=true; srg.Gr(4,[6 9])=true; 
srg.Gr(5,8)=true; srg.Gr(6,[8 9])=true; srg.Gr(7,9)=true; 

rg{1}=srg;

end

