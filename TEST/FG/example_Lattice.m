function  [fg, regions, regions_jt1, rg_jt2 rg_exact] = example_Lattice()
% Evaluations :
%    - fg : factor fraph for the 3x3 lattice example of Yedidia Freeman and Weiss
%    - regions : some regions
%    - regions_jt1 : regions corresponding of graph slices 
%    - rg_jt2 : region graph for JT behavior
%    - rg_exact : region graph for exact resolution

% Lattice variables and factors:
% 1-A-2-B-3
% |   |   |
% C   D   E
% |   |   |
% 4-F-5-G-6
% |   |   |
% H   I   J
% |   |   |
% 7-K-8-L-9

G = logical( [1 1 0 0 0 0 0 0 0;
              0 1 1 0 0 0 0 0 0;
              1 0 0 1 0 0 0 0 0;
              0 1 0 0 1 0 0 0 0;
              0 0 1 0 0 1 0 0 0;
              0 0 0 1 1 0 0 0 0;
              0 0 0 0 1 1 0 0 0;
              0 0 0 1 0 0 1 0 0;
              0 0 0 0 1 0 0 1 0;
              0 0 0 0 0 1 0 0 1;
              0 0 0 0 0 0 1 1 0;
              0 0 0 0 0 0 0 1 1] );
for f=1:size(G,1); eval(['sfg.F{' int2str(f) '}.V = find(G(' int2str(f) ',:));']); end

fg.Card = [2 3 4 4 2 3 3 4 2];

sfg.Vcode = 1:9;
sfg.F{1}.T = [0.5 0.3 0.2 ; 0.2 0.2 0.6];
sfg.F{2}.T = [0.1 0.4 0.3 0.2 ; 0.2 0.2 0.3 0.3 ; 0.4 0.2 0.3 0.1];
sfg.F{3}.T = [0.2 0.3 0.2 0.1 ; 0.3 0.25 0.25 0.2];
sfg.F{4}.T = [0.2 0.8 ; 0.8 0.2 ; 0.5 0.5];
sfg.F{5}.T = [0.5 0.3 0.2 ; 0.2 0.2 0.6 ; 0.4 0.3 0.3 ; 0.1 0.3 0.6];
sfg.F{6}.T = [0.4 0.6 ; 0.2 0.8 ; 0.5 0.5 ; 0.3 0.7];
sfg.F{7}.T = [0.5 0.3 0.2 ; 0.2 0.2 0.6];
sfg.F{8}.T = [0.5 0.3 0.2 ; 0.2 0.2 0.6 ; 0.4 0.3 0.3 ; 0.1 0.3 0.6];
sfg.F{9}.T = [0.2 0.3 0.2 0.1 ; 0.3 0.25 0.25 0.2];
sfg.F{10}.T = [0.2 0.8 ; 0.8 0.2 ; 0.5 0.5];
sfg.F{11}.T = [0.1 0.4 0.3 0.2 ; 0.2 0.2 0.3 0.3 ; 0.4 0.2 0.3 0.1];
sfg.F{12}.T = [0.4 0.6 ; 0.2 0.8 ; 0.5 0.5 ; 0.3 0.7];

sfg.BG = setBG(length(fg.Card),sfg.F);
fg.sfg{1}=sfg;


% Regions
regions{1} = logical([1 1 0 1 1 0 0 0 0 1 0 1 1 0 1 0 0 0 0 0 0;
                   0 1 1 0 1 1 0 0 0 0 1 0 1 1 0 1 0 0 0 0 0;
                   0 0 0 1 1 0 1 1 0 0 0 0 0 0 1 0 1 1 0 1 0;
                   0 0 0 0 1 1 0 1 1 0 0 0 0 0 0 1 0 1 1 0 1]);
        
               
% Regions corresponding to slices of the graph
regions_jt1{1} = false(2,21);
I_r1 = [1 2 3 4 5 6 10 11 12 13 14 15 16];
regions_jt1{1}(1,I_r1) = true;
I_r2 = [4 5 6 7 8 9 15 16 17 18 19 20 21];
regions_jt1{1}(2,I_r2) = true;


% Regions graph corresponding to JT
regions_jt2 = false(11,21);
I_r1 = [1 2 4 10 12];
regions_jt2(1,I_r1) = true;
I_r2 = [2 3 4 5 11 13];
regions_jt2(2,I_r2) = true;
I_r3 = [3 4 5 6 14];
regions_jt2(3,I_r3) = true;
I_r4 = [4 5 6 7 15 17];
regions_jt2(4,I_r4) = true;
I_r5 = [5 6 7 8 16 18];
regions_jt2(5,I_r5) = true;
I_r6 = [6 7 8 9 19 20 21];
regions_jt2(6,I_r6) = true;
I_r7 = [2 4];
regions_jt2(7,I_r7) = true;
I_r8 = [3 4 5];
regions_jt2(8,I_r8) = true;
I_r9 = [4 5 6];
regions_jt2(9,I_r9) = true;
I_r10 = [5 6 7];
regions_jt2(10,I_r10) = true;
I_r11 = [6 7 8];
regions_jt2(11,I_r11) = true;

srg_jt2.Vr=regions_jt2(:, 1:9);
srg_jt2.Fr=regions_jt2(:, 10:end);
srg_jt2.Gr = false(11,11);
srg_jt2.Gr(1,7) = true;
srg_jt2.Gr(2,7) = true;
srg_jt2.Gr(2,8) = true;
srg_jt2.Gr(3,8) = true;
srg_jt2.Gr(3,9) = true;
srg_jt2.Gr(4,9) = true;
srg_jt2.Gr(4,10) = true;
srg_jt2.Gr(5,10) = true;
srg_jt2.Gr(5,11) = true;
srg_jt2.Gr(6,11) = true;
rg_jt2{1}=srg_jt2;

% Region graphe with one region for exact resolution
srg_exact.Vr=true(1,9);
srg_exact.Fr=true(1,12);
srg_exact.Gr = false;
rg_exact{1}=srg_exact;
end



