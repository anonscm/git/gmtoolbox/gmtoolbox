function fg = example_Tiny()

fg.Card = [2 2];

% Define one sub factor graph
sfg.Vcode= 1:2;
f.V = [1 2];  
f.T = [0.2 0.4; 
       0.1 0.3];    
sfg.F = {f};
sfg.BG = setBG(2, sfg.F);

fg.sfg{1}=sfg;
end

