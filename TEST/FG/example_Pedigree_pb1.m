function fg = example_pedigree_pb()

fg.Card=[2 3 4];
sfg.Vcode=[1 2 3];
F{1}.V=[2 1];
F{1}.T=eye(3,2);
F{2}.V=[3 2];
F{2}.T=eye(4,3);
F{3}.V=[1 2 3];
x(1,:,:)=eye(3,4);
x(2,:,:)=eye(3,4);
F{3}.T=x;
sfg.F=F;
fg.sfg{1}=sfg;
fg.sfg{1}.BG=setBG(3,F);
%[is_OK, is_disconnected, msg] = gm_check_fg( fg )

end
