function fg = example_YedidiaFig4()


fg.Card = [2 2 2 2 2 2 2 2 2]; % cardinalities of 9 variables

sfg.Vcode=1:9;
FA.V = [1 2 4 5];
FA.T = @fA;
FB.V = [2 3 5 6];
FB.T = @fB;
FC.V = [4 5];
FC.T = @fC;
FD.V = [5 6];
FD.T = @fD;
FE.V = [4 5 7 8];
FE.T = @fE;
FF.V = [5 6 8 9];
FF.T = @fF;
sfg.F ={FA, FB, FC, FD, FE, FF}; % factors

sfg.BG = setBG(9,sfg.F);

fg.sfg{1}=sfg;
end


function cpt = fA
   s=2; % number of states for all variables
   rng(1); cpt = rand(s*ones(1,4));
   for i1=1:s; for i2=1:s; for i3=1:s; k=cpt(i1,i2,i3,:); cpt(i1,i2,i3,:)= k/sum(k); end;end; end
end
function cpt = fB
   s=2; % number of states for all variables
   rng(2); cpt = rand(s*ones(1,4));
   for i1=1:s; for i2=1:s; for i3=1:s; k=cpt(i1,i2,i3,:); cpt(i1,i2,i3,:)= k/sum(k); end;end; end
end
function cpt = fC
  s=2; % number of states for all variables
  rng(3); cpt = rand(s*ones(1,2));
  for i1=1:s; k=cpt(i1,:); cpt(i1,:)= k/sum(k); end
end
function cpt = fD
   s=2; % number of states for all variables
   rng(4); cpt = rand(s*ones(1,2));
  for i1=1:s; k=cpt(i1,:); cpt(i1,:)= k/sum(k); end
end   
function cpt = fE
  s=2; % number of states for all variables
  rng(5); cpt = rand(s*ones(1,4));
   for i1=1:s; for i2=1:s; for i3=1:s; k=cpt(i1,i2,i3,:); cpt(i1,i2,i3,:)= k/sum(k); end;end; end
end   
function cpt = fF
   s=2; % number of states for all variables
   rng(6); cpt = rand(s*ones(1,4));
   for i1=1:s; for i2=1:s; for i3=1:s; k=cpt(i1,i2,i3,:); cpt(i1,i2,i3,:)= k/sum(k); end;end; end
end 

