function [fg, regions] = example_Trivial()
% Trivial case : P2(x2|x1).P1(x1)

fg.Card = [2 2];

% Define one sub factor graph
sfg.Vcode= 1:2;
f.V=1; f.T=[0.4; 0.6];
sfg.F{1} = f;
f.V=[1 2]; f.T=[0.2, 0.1; 
                0.6, 0.1];
sfg.F{2} = f;

sfg.BG = setBG( 2, sfg.F);
fg.sfg{1}=sfg;


% Valid regions
regions{1} = logical([1 0 1 0 ;  % R1 = {x1,P1}
                   1 1 0 1]); % R2 = {x1, x2, P2}

end