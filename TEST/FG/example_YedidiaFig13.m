function fg = example_YedidiaFig13()


fg.Card = [2 2 2 2 2 2 2]; % cardinalities of 7 variables

sfg.Vcode=1:length(fg.Card);
FA.V = [1 2 3 5];
FA.T = @fA;
FB.V = [1 2 4 6];
FB.T = @fB;
FC.V = [1 3 4 7];
FC.T = @fC;
F1.V = 1;
F1.T = @f1;
F2.V = 2;
F2.T = @f2;
F3.V = 3;
F3.T = @f3;
F4.V = 4;
F4.T = @f4;
F5.V = 5;
F5.T = @f5;
F6.V = 6;
F6.T = @f6;
F7.V = 7;
F7.T = @f7;
sfg.F ={F1, F2, F3, F4, F5, F6, F7, FA, FB, FC}; % factors
sfg.BG = setBG(length(sfg.Vcode), sfg.F);

fg.sfg{1}=sfg;


end



function cpt = fA
   s=2; % number of states for all variables
   rng(10); cpt = rand(s*ones(1,4));
   for i1=1:s; for i2=1:s; for i3=1:s; k=cpt(i1,i2,i3,:); cpt(i1,i2,i3,:)= k/sum(k); end;end; end
end
function cpt = fB
   s=2; % number of states for all variables
   rng(20); cpt = rand(s*ones(1,4));
   for i1=1:s; for i2=1:s; for i3=1:s; k=cpt(i1,i2,i3,:); cpt(i1,i2,i3,:)= k/sum(k); end;end; end
end
function cpt = fC
  s=2; % number of states for all variables
  rng(30); cpt = rand(s*ones(1,4));
  for i1=1:s; k=cpt(i1,:); cpt(i1,:)= k/sum(k); end
end
function cpt = f1
  s=2; % number of states for all variables
  rng(1); cpt = rand([s 1]);
  cpt = cpt / sum(cpt);
end
function cpt = f2
  s=2; % number of states for all variables
  rng(2); cpt = rand([s 1]);
  cpt = cpt / sum(cpt);
end
function cpt = f3
  s=2; % number of states for all variables
  rng(3); cpt = rand([s 1]);
  cpt = cpt / sum(cpt);
end
function cpt = f4
  s=2; % number of states for all variables
  rng(4); cpt = rand([s 1]);
  cpt = cpt / sum(cpt);
end
function cpt = f5
  s=2; % number of states for all variables
  rng(5); cpt = rand([s 1]);
  cpt = cpt / sum(cpt);
end
function cpt = f6
  s=2; % number of states for all variables
  rng(6); cpt = rand([s 1]);
  cpt = cpt / sum(cpt);
end
function cpt = f7
  s=2; % number of states for all variables
  rng(7); cpt = rand([s 1]);
  cpt = cpt / sum(cpt);
end


