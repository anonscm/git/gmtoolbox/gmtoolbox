clear all

disp('***** Test UAI functions *****')

% GMtoolbox examples write and read
% for UAI format, see http://www.cs.huji.ac.il/project/PASCAL/fileFormat.php
fg1 = gm_example_Sprinkler;
gm_write_fg(fg1, 'Sprinkler.uai');
fg2 = gm_read_fg('EXPECTED/UAI_Sprinkler.uai');
if compareFG( fg1, fg2 ); disp('Test Sprinkler: OK'); else; disp('Test Sprinkler: PB'); end
system('rm Sprinkler.uai');

fg1 = gm_example_Ecology;
gm_write_fg(fg1, 'Ecology.uai');
fg2 = gm_read_fg('EXPECTED/UAI_Ecology.uai');
if compareFG( fg1, fg2 ); disp('Test Ecology: OK'); else; disp('Test Ecology: PB'); end
system('rm Ecology.uai');

fg1 = gm_example_CHMM( 4, 3);
gm_write_fg(fg1, 'CHMM_N4_T3.uai');
fg2 = gm_read_fg('EXPECTED/UAI_CHMM_N4_T3.uai');
if compareFG( fg1, fg2 ); disp('Test CHMM_N4_T3: OK'); else; disp('Test CHMM_N4_T3: PB'); end
system('rm CHMM_N4_T3.uai');

global NS; NS=3; fg1 = gm_example_DBN( 4, 5);
gm_write_fg(fg1, 'DBN_NS3_nv4_t5.uai');
fg2 = gm_read_fg('EXPECTED/UAI_DBN_NS3_nv4_t5.uai');
if compareFG( fg1, fg2 ); disp('Test DBN_NS3_nv4_t5: OK'); else; disp('Test DBN_NS3_nv4_t5: PB'); end
system('rm DBN_NS3_nv4_t5.uai');


% Read other UAI files in ../BENCHMARK/PASCAL2/
% http://www.cs.huji.ac.il/project/PASCAL/showExample.php
fg = gm_read_fg('FG/Family2Dominant.1.5loci.uai');
if gm_check_fg(fg); disp('Test Family2Dominant.1.5loci: OK'); else; disp('Test Family2Dominant.1.5loci: PB'); end

fg = gm_read_fg('FG/grid3x3.uai');
if gm_check_fg(fg); disp('Test grid3x3: OK'); else; disp('Test grid3x3: PB'); end

fg = gm_read_fg('FG/grid4x4.uai');
if gm_check_fg(fg); disp('Test grid4x4: OK'); else; disp('Test grid4x4: PB'); end

fg = gm_read_fg('FG/smokers_10.uai');
if gm_check_fg(fg); disp('Test smokers_10: OK'); else; disp('Test smokers_10: PB'); end

% Write evidence
evidence = [0 1 1 0]; % no sprinkler, no rain
gm_write_evidence( evidence, 'test.uai.evid' );
evidence_expected = gm_read_evidence( 'EXPECTED/UAI_Sprinkler.uai.evid' , 4);
if all(abs(evidence-evidence_expected)<0.0001); disp('Test evidence 1: OK'); else; disp('Test evidence 1: PB'); end
system('rm test.uai.evid');

fg = gm_example_Sprinkler;
rg = gm_rg_CVM(fg);

% Write MAR
b = gm_infer_GBP(fg,rg);
gm_write_MAR( b, 'test.uai.MAR' );
b_expected = gm_read_MAR( 'EXPECTED/UAI_Sprinkler.uai.MAR');
is_OK = true;
for i=1:max(length(b),length(b_expected))
    if any(abs(b{i}-b_expected{i})>0.001)
        is_OK = false;
    end
end
if is_OK; disp('Test marginal 1: OK'); else; disp('Test marginal 1: PB'); end
system('rm test.uai.MAR');


% Write regions belief
[b, stop_by, br] = gm_infer_GBP ( fg , rg);
gm_write_BEL(br, 'test.uai.CVM.BEL');
br_expected = gm_read_BEL('EXPECTED/UAI_Sprinkler.uai.CVM.BEL', fg, rg);
is_OK = true;
for i=1:max(length(br),length(br_expected))
    if any(abs(reshape(br{1}{i},1,numel(br{1}{i}))-reshape(br_expected{1}{i},1,numel(br_expected{1}{i})))>0.001)
        is_OK = false;
    end
end
if is_OK; disp('Test belief 1: OK'); else; disp('Test belief 1: PB'); end
system('rm test.uai.CVM.BEL');

