function is_OK = compareRG (rg1, rg2)
is_OK = true;

if length(rg1)~=length(rg2); is_OK=false; end

for irg=1:length(rg1)
    if any(any(rg1{irg}.Vr~=rg2{irg}.Vr)); is_OK=false; end
    if any(any(rg1{irg}.Fr~=rg2{irg}.Fr)); is_OK=false; end
    if any(any(rg1{irg}.Gr~=rg2{irg}.Gr)); is_OK=false; end
    if any(any(rg1{irg}.cr~=rg2{irg}.cr)); is_OK=false; end
end

end