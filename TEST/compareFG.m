function [is_equal, msg] = compareFG( fg1, fg2 )
% Check if 2 factors graphs are equal, ignoring nodes names (fg.Names)
% precision allows to compare probabilities values (take 0.0001 when
% one factor graph wax write on file with gm_write_fg function)

precision = 0.0001;

is_equal =true;
msg = {}; nb_msg = 1;

if all(fg1.Card ~= fg2.Card) 
    is_equal = false; msg{nb_msg}='Card (fg.Card) different.'; nb_msg=nb_msg+1;
end

if length(fg1.sfg)~=length(fg2.sfg)
    is_equal = false; msg{nb_msg}='Number of sub factor graph (fg.sfg) different.'; nb_msg=nb_msg+1;
else
    for i=1:length(fg1.sfg)
        if any(fg1.sfg{i}.Vcode~=fg2.sfg{i}.Vcode)
            is_equal = false; msg{nb_msg}=['In sub factor graph ' int1str(i) ', Vcode (fg.sfg{' int1str(i) '}.Vcode) different.']; nb_msg=nb_msg+1;
        end
        if any(any(fg1.sfg{i}.BG~=fg2.sfg{i}.BG))
            is_equal = false; msg{nb_msg}=['In sub factor graph ' int1str(i) ', BG (fg.sfg{' int1str(i) '}.BG) different.']; nb_msg=nb_msg+1;
        end
        if length(fg1.sfg{i}.F)~=length(fg2.sfg{i}.F)
            is_equal = false; msg{nb_msg}=['In sub factor graph ' int1str(i) ', number of factors (fg.sfg{' int1str(i) '}.F) different.']; nb_msg=nb_msg+1;
        else
            for f=1:length(fg1.sfg{i}.F)
               if any(fg1.sfg{i}.F{f}.V ~= fg2.sfg{i}.F{f}.V)
                   is_equal = false; msg{nb_msg}=['In sub factor graph ' int2str(i) ' and factor ' int2str(f) ', V (fg.sfg{' int2str(i) '}.F{' int2str(f) '}.V) different.']; nb_msg=nb_msg+1;
               end
               T1 = fg1.sfg{i}.F{f}.T; if  isa(T1, 'function_handle'); T1 = T1();end
               T2 = fg2.sfg{i}.F{f}.T; if  isa(T2, 'function_handle'); T2 = T2();end
               if any(size(T1)~=size(T2))
                  is_equal = false; msg{nb_msg}=['In sub factor graph ' int2str(i) ' and factor ' int2str(f) ', tables (fg.sfg{' int2str(i) '}.F{' int2str(f) '}.T) with different size.']; nb_msg=nb_msg+1;
               else
                  if any(abs(reshape(T1,1,numel(T1))-reshape(T2,1,numel(T2)))>precision)
                    is_equal = false; msg{nb_msg}=['In sub factor graph ' int2str(i) ' and factor ' int2str(f) ', tables (fg.sfg{' int2str(i) '}.F{' int2str(f) '}.T) different.']; nb_msg=nb_msg+1;                   
                  end
               end
            end
        end
    end
end

end


