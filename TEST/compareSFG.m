function is_OK = compareSFG (sfg1, sfg2)
is_OK = true;

    if any(sfg1.Vcode~=sfg2.Vcode); is_OK=false; end
    if length(sfg1.F)~=length(sfg2.F); is_OK=false; end
    
    for f=1:length(sfg1.F)
        if any(sfg1.F{f}.V~=sfg2.F{f}.V)
            is_OK=false;
        elseif any( abs(reshape(sfg1.F{f}.T,1,numel(sfg1.F{f}.T)) - ...
                reshape(sfg2.F{f}.T,1,numel(sfg2.F{f}.T))) > 0.0001)
            is_OK=false;
        end
    end

    if any(any(sfg1.BG ~= sfg2.BG)); is_OK=false; end

end