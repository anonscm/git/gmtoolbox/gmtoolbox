function [D, M, deltaM] = Test_DBN_libDAI( NV, H)
% compare GMtoolbox with libDAI
% Example: 
% global NS; NS=3;NV=2;H=3;[D, M, deltaM] = Test_DBN_libDAI(NV, H)
% d=D.GMtoolbox-D.libDAI

global NS;
% run GMtoolbox
addpath(genpath('../gmtoolbox'));
fg = gm_example_DBN(NV, H);
rg = gm_rg_CVM(fg);
tic; M.GMtoolbox = gm_infer_GBP(fg, rg); D.GMtoolbox=toc;


% transform a GMtoolbox DBN example factor graph in a libDAI factorgraph
% The maximum size of tables is 3.
sfg=fg.sfg{1};
psi=cell(1,length(sfg.F));
for i=1:length(sfg.F)
    f=sfg.F{i};
    ff.Member=f.V-1; % variable number stars at zero
    P = f.T();
    if length(size(P))==1 
        ff.P = P;
    elseif length(size(P))==2
        ff.P = mylinearize(P);
    elseif length(size(P))==3
        ff.P=[]; for j=1:size(P,1); ff.P = [ff.P mylinearize(P(j,:,:))]; end
    else
        disp('ERROR in fgtofgdai: dimension of P is higher than 3'); 
    end
    psi{i}=ff;
end
% write on disk in UAI format
fid = fopen('dbn.uai','w');
fprintf(fid,'BAYES\n');
fprintf(fid,'%i\n',length(fg.Card)); % number of variables
fprintf(fid,'%i ',fg.Card); fprintf(fid,'\n'); % cardinalities
fprintf(fid,'%i\n',length(sfg.F)); % number of factors
for i=1:length(psi);fprintf(fid,'%i ',length(psi{i}.Member));fprintf(fid,'%i ',psi{i}.Member);fprintf(fid,'\n');end % indexes of variables, starting at 0
for i=1:length(psi);fprintf(fid,'%i\n',length(psi{i}.P));fprintf(fid,'%1.4f ',psi{i}.P);fprintf(fid,'\n');end % tables 
fclose(fid);
fid = fopen('dbn.uai.evid','w');
fprintf(fid,'0\n');
fclose(fid);
% convert in libDAI format execute BP
%system('sh DBN_libDAI.sh');
disp('Execute "sh Test_DBN_libDAI.sh" then type return.');
pause

fid = fopen('bp.time.txt');
c=textscan(fid,'%s'); c=c{1}; 
D.libDAI=str2double(c{3}(3:(end-7)));
fclose(fid);
fid = fopen('bp.marg.txt');
c=textscan(fid,'%s'); c=c{1};
k=1;
for v=1:NV*(H+1)
    k=k+2;
    m=zeros(NS,1);
    m(1) = str2double(c{k}(2:(end-1))); k=k+1;
    for s=2:NS-1; m(s) = str2double(c{k}(1:(end-1))); k=k+1;end
    m(NS) = str2double(c{k}(1:(end-2))); k=k+1;
    M.libDAI{v} = m;
end
fclose(fid);    

deltaM=0; for i=1:length(M.GMtoolbox); deltaM = max(deltaM, max(abs(M.GMtoolbox{i}-M.libDAI{i})));end; 
end


function m = mylinearize(M)
% transform a matrix in a vector following libDAI request
m = squeeze(M)';
m = reshape(m,1, numel(m));
end
