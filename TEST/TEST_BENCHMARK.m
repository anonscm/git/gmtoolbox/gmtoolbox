% TEST BENCHMARK

% output_file_name = 'TIME_perdu_easy.txt';
% dir_fg = '../BENCHMARK/cost-function-library/real/ergo/instances/uai/easy/';
% names_fg = {'alarm.uai','andes.uai','asia.uai','cancer.uai','cpcs54.uai', ...
%    'diagnose_a.uai','diagnose_b.uai','emdec6g.uai','fire_alarm.uai', ...
%    'hepar2.uai','pathfinder.uai','sachs.uai','spect.uai','tcc4e.uai','win95pts.uai'};


% output_file_name = 'TIME_perdu_ergo.txt';
% dir_fg = '../BENCHMARK/cost-function-library/real/ergo/instances/uai/';
% % PB: 'munin1.uai', 'munin2.uai', 'munin3.uai', 'munin4.uai' variable(s) with cardinality 1
% names_fg = {'barley.uai','diabetes.uai','link.uai','mildew.uai','pigs.uai','water.uai'};


% output_file_name = 'TIME_perdu_pascal2.txt';
% dir_fg = '../BENCHMARK/PASCAL2/';
% % Out of memory for inference: 'protein1.uai','protein2.uai','protein3.uai',
% 'protein4.uai','protein5.uai','protein6.uai','protein7.uai','protein8.uai','pdb1i24.uai'
% names_fg = {'10_14_s.binary.uai','30markers.uai','deer_rescaled_0034.K10.F1.25.model.uai', ...
%      'Family2Dominant.1.5loci.uai','GEOM30a_3.uai','grid10x10.f2.wrap.uai', ...
%      'grid3x3.uai','grid4x4.uai','network.uai','or_chain_10.fg.uai','smokers_10.uai'};


% output_file_name = 'TIME_perdu_toulbar2.txt';
% dir_fg = '../BENCHMARK/toulbar2/';
% Do not achieve on PC perdu : ,'pedigree9_GMtoolbox.uai' (OK on server sullo)
% names_fg = {'1CM1.uai', 'GeomSurf-7-gm256.uai'};


% output_file_name = 'test.txt';
% dir_fg = '../BENCHMARK/cost-function-library/crafted/UAI08/bn2o/instances/uai/';
% names_fg = {'bn2o-30-15-150-1a.uai'};
% dir_fg = '../BENCHMARK/cost-function-library/crafted/UAI08/relational/instances/uai/';
% names_fg = {'blockmap_05_01-0000.uai'};
% dir_fg = '../BENCHMARK/cost-function-library/random/UAI08/grids/50/instances/uai/';
% names_fg = {'50-12-10.uai'};



for i=1:length(names_fg)
    disp(names_fg{i});
    tic; fg = gm_read_fg([dir_fg names_fg{i}]); t(1)=toc;
    save([dir_fg names_fg{i} '.fg.mat'],'fg');
    if ~gm_check_fg(fg); disp(['PROBLEM with ' name_fg{i}]); end
    tic; rg = gm_rg_CVM(fg); t(2)=toc;
    tic; [b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false); t(3)=toc;
    save(output_file_name, 't', '-ascii', '-append');
end


