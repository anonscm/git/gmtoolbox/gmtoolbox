% Test gm_infer_GBP.m

disp('***** Test gm_infer_GBP.m *****')
clear fg rg b stop_by b_expected stop_by_expected;

% Limit case
fg = example_Tiny();
rg = gm_rg_CVM(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, true);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP1.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP1.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Tiny 1: OK'); 
else disp('Test Tiny 1: PB'); is_OK=false; end

evidence = [1 1]; % all variable observed
fge = gm_include_evidence(fg, evidence); 
rge = gm_rg_CVM(fge);
[b, stop_by] = gm_infer_GBP( fge , rge, 100, 0.0001, 0.0, false);
b_expected{1}=[1;0]; b_expected{2}=[1;0]; stop_by_expected=[];
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Tiny 2: OK'); 
else disp('Test Tiny 2: PB'); is_OK=false; end

clear fg rg b stop_by b_expected stop_by_expected;


%% Trivial case : P2(x2|x1).P1(x1)
[fg, regions] = example_Trivial();
rg = gm_rg_CVM(fg, regions); 
[b1, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b1; stop_by_expected=stop_by; save('EXPECTED/GBP10.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP10.mat')
if compareGBP(b1,stop_by, b_expected,stop_by_expected); disp('Test Trivial 1: OK'); 
else disp('Test Trivial 1: PB'); is_OK=false; end

srg_exact.Vr=true(1,length(fg.Card)); srg_exact.Fr=true(1,length(fg.sfg{1}.F)); srg_exact.Gr=false; srg_exact.cr=setCountingNumbers(srg_exact.Gr);
rg_exact{1}=srg_exact;
[b2, stop_by] = gm_infer_GBP( fg , rg_exact, 100, 0.0001, 0.0, false);
%b_expected=b2; stop_by_expected=stop_by; save('EXPECTED/GBP11.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP11.mat')
if compareGBP(b2,stop_by, b_expected,stop_by_expected); disp('Test Trivial 2: OK'); 
else disp('Test Trivial 2: PB'); is_OK=false; end

% Comparison of the marginals: should both be exact (the rg is trivially a tree.
if compareGBP(b1,stop_by,b2,stop_by); disp('Test Trivial 3: OK'); 
else disp('Test Trivial 3: PB'); is_OK=false; end
clear fg rg  regions srg_exact rg_exact b1 br b2 stop_by b_expected stop_by_expected;



%% Sprinkler case
fg = gm_example_Sprinkler();

rg = gm_rg_CVM( fg );
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP20.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP20.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Sprinkler 1: OK'); 
else disp('Test Sprinkler 1: PB'); is_OK=false; end

% Init regions using nice ones:
regions{1} = logical([1 1 1 0 1 1 1 0 ;
                   0 1 1 1 0 0 0 1]);
rg = gm_rg_CVM( fg, regions);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP21.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP21.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Sprinkler 2: OK'); 
else disp('Test Sprinkler 2: PB'); is_OK=false; end

rg = gm_rg_JT( fg );
[b1, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b1; stop_by_expected=stop_by; save('EXPECTED/GBP22.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP22.mat')
if compareGBP(b1,stop_by, b_expected,stop_by_expected); disp('Test Sprinkler 3: OK'); 
else disp('Test Sprinkler 3: PB'); is_OK=false; end


% With one single full region... In this case, calculus is exact (but quickly infeasible!)...
srg_exact.Vr=true(1,length(fg.Card)); srg_exact.Fr=true(1,length(fg.sfg{1}.F)); srg_exact.Gr=false; srg_exact.cr=setCountingNumbers(srg_exact.Gr);
rg_exact{1}=srg_exact;
[b2, stop_by] = gm_infer_GBP( fg , rg_exact, 100, 0.0001, 0.0, false);
%b_expected=b2; stop_by_expected=stop_by; save('EXPECTED/GBP23.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP23.mat')
if compareGBP(b2,stop_by, b_expected,stop_by_expected); disp('Test Sprinkler 4: OK'); 
else disp('Test Sprinkler 4: PB'); is_OK=false; end

% Comparison of the marginals: should both be exact (the rg is trivially a tree.
if compareGBP(b1,stop_by,b2,stop_by); disp('Test Sprinkler 4: OK'); 
else disp('Test Sprinkler 4: PB'); is_OK=false; end
clear fg rg  regions srg_exact rg_exact b b1 br b2 stop_by b_expected stop_by_expected;




%% 3 x3 lattice case
[fg, regions, regions_jt1, rg_jt2] = example_Lattice();

rg = gm_rg_CVM( fg );
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP30.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP30.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Lattice 1: OK'); 
else disp('Test Lattice 1: PB'); is_OK=false; end

rg = gm_rg_CVM( fg, regions);
[b, stop_by] = gm_infer_GBP( fg , rg, 200, 0.0001, 0.9, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP31.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP31.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Lattice 2: OK'); 
else disp('Test Lattice 2: PB'); is_OK=false; end

rg = gm_rg_CVM( fg, regions_jt1);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP32.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP32.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Lattice 3: OK'); 
else disp('Test Lattice 3: PB'); is_OK=false; end

[b, stop_by] = gm_infer_GBP( fg , rg_jt2, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP33.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP33.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Lattice 4: OK'); 
else disp('Test Lattice 4: PB'); is_OK=false; end

rg = gm_rg_BETHE( fg );
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP34.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP34.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Lattice 5: OK'); 
else disp('Test Lattice 5: PB'); is_OK=false; end

rg = gm_rg_JT( fg );
[b1, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b1; stop_by_expected=stop_by; save('EXPECTED/GBP35.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP35.mat')
if compareGBP(b1,stop_by, b_expected,stop_by_expected); disp('Test Lattice 6: OK'); 
else disp('Test Lattice 6: PB'); is_OK=false; end

srg_exact.Vr=true(1,length(fg.Card)); srg_exact.Fr=true(1,length(fg.sfg{1}.F)); srg_exact.Gr=false; rg_exact{1}=srg_exact;
[b2, stop_by] = gm_infer_GBP( fg , rg_exact, 100, 0.0001, 0.0, false);
%b_expected=b2; stop_by_expected=stop_by; save('EXPECTED/GBP37.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP37.mat')
if compareGBP(b2,stop_by, b_expected,stop_by_expected); disp('Test Lattice 7: OK'); 
else disp('Test Lattice 7: PB'); is_OK=false; end

% Comparison of the marginals: should both be exact (the rg is trivially a tree.
if compareGBP(b1,stop_by,b2,stop_by); disp('Test Lattice 8: OK'); 
else disp('Test Lattice 8: PB'); is_OK=false; end

clear fg rg regions regions_jt1 rg_jt2 srg_exact rg_exact b b1 b2 br stop_by b_expected stop_by_expected;



%% Case Yedidia Figure 4
fg = example_YedidiaFig4();

rg = gm_rg_CVM(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.9, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP40.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP40.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Figure4 1: OK'); 
else disp('Test Figure4 1: PB'); is_OK=false; end

rg = gm_rg_JT(fg);
[b1, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b1; stop_by_expected=stop_by; save('EXPECTED/GBP41.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP41.mat')
if compareGBP(b1,stop_by, b_expected,stop_by_expected); disp('Test Figure4 2: OK'); 
else disp('Test Figure4 2: PB'); is_OK=false; end

% With one single full region
srg_exact.Vr=true(1,length(fg.Card)); srg_exact.Fr=true(1,length(fg.sfg{1}.F)); srg_exact.Gr=false; rg_exact{1}=srg_exact;
[b2, stop_by] = gm_infer_GBP( fg , rg_exact, 100, 0.0001, 0.0, false);
%b_expected=b2; stop_by_expected=stop_by; save('EXPECTED/GBP42.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP42.mat')
if compareGBP(b2,stop_by, b_expected,stop_by_expected); disp('Test Figure4 3: OK'); 
else disp('Test Figure4 3: PB'); is_OK=false; end

% Comparison of the marginals: should both be exact (the rg is trivially a tree.
if compareGBP(b1,stop_by,b2,stop_by); disp('Test Figure4 4: OK'); 
else disp('Test Figure4 4: PB'); is_OK=false; end
clear fg rg  regions srg_exact rg_exact b b1 br b2 stop_by b_expected stop_by_expected;




%% Case Yedidia Figure 13
fg = example_YedidiaFig13();

rg = gm_rg_CVM(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP50.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP50.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Figure13 1: OK'); 
else disp('Test Figure13 1: PB'); is_OK=false; end

rg = gm_rg_BETHE(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP51.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP51.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Figure13 2: OK'); 
else disp('Test Figure13 2: PB'); is_OK=false; end

rg = gm_rg_JT(fg);
[b1, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b1; stop_by_expected=stop_by; save('EXPECTED/GBP52.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP52.mat')
if compareGBP(b1,stop_by, b_expected,stop_by_expected); disp('Test Figure13 3: OK'); 
else disp('Test Figure13 3: PB'); is_OK=false; end

% With one single full region
srg_exact.Vr=true(1,length(fg.Card)); srg_exact.Fr=true(1,length(fg.sfg{1}.F)); srg_exact.Gr=false; rg_exact{1}=srg_exact;
[b2, stop_by] = gm_infer_GBP( fg , rg_exact, 100, 0.0001, 0.0, false);
%b_expected=b2; stop_by_expected=stop_by; save('EXPECTED/GBP53.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP53.mat')
if compareGBP(b2,stop_by, b_expected,stop_by_expected); disp('Test Figure13 4: OK'); 
else disp('Test Figure13 4: PB'); is_OK=false; end

% Comparison of the marginals: should both be exact (the rg is trivially a tree.
if compareGBP(b1,stop_by,b2,stop_by); disp('Test Figure13 5: OK'); 
else disp('Test Figure13 5: PB'); is_OK=false; end
clear fg rg srg_exact rg_exact b b1 br b2 stop_by b_expected stop_by_expected;




%% Case DBN
global NS; NS=3; fg = gm_example_DBN(5,5);

rg = gm_rg_CVM(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false); % stopped by epsilon
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP60.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP60.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test DBN 5-3-5 1: OK'); 
else disp('Test DBN 5-3-5 1: PB'); is_OK=false; end

rg = gm_rg_BETHE(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP61.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP61.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test DBN 5-3-5 2: OK'); 
else disp('Test DBN 5-3-5 2: PB'); is_OK=false; end

rg = gm_rg_JT(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP62.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP62.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test DBN 5-3-5 3: OK'); 
else disp('Test DBN 5-3-5 3: PB'); is_OK=false; end

clear fg rg rg_exact b stop_by b_expected stop_by_expected NS;



%% Case CHMM
fg = gm_example_CHMM(9,3);

rg = gm_rg_CVM(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false); % stopped by Nmax
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP70.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP70.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test CHMM 1: OK'); 
else disp('Test CHMM 1: PB'); is_OK=false; end

rg = gm_rg_JT(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP71.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP71.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test CHMM 2: OK'); 
else disp('Test CHMM 2: PB'); is_OK=false; end

rg = gm_rg_BETHE(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP72.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP72.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test CHMM 3: OK'); 
else disp('Test CHMM 3: PB'); is_OK=false; end

clear fg rg rg_exact b stop_by b_expected stop_by_expected;



%% Case with special N and D
[fg, rg0] = example_YedidiaFig11();

[b, stop_by] = gm_infer_GBP( fg , rg0, 100, 0.0001, 0.0, false); % stopped by Nmax
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP80.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP80.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Figure11 1: OK'); 
else disp('Test Figure11 1: PB'); is_OK=false; end

rg = gm_rg_CVM(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP81.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP81.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Figure11 2: OK'); 
else disp('Test Figure11 2: PB'); is_OK=false; end

rg = gm_rg_BETHE(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP82.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP82.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Figure11 3: OK'); 
else disp('Test Figure11 3: PB'); is_OK=false; end

rg = gm_rg_JT(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP83.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP83.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Figure11 4: OK'); 
else disp('Test Figure11 4: PB'); is_OK=false; end

[b, stop_by] = gm_infer_GBP( fg , rg0, 500, 0.0001, 0.5, false); % stopped by epsilon
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP84.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP84.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Figure11 5: OK'); 
else disp('Test Figure11 5: PB'); is_OK=false; end

clear fg rg0 rg  b stop_by b_expected stop_by_expected;


%% Case with degraded BENCHMARK pedigree9_GMtoolbox.uai factor graph
fg = example_Pedigree_pb1();
rg = gm_rg_CVM(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP91.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP91.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Pedigree pb 1: OK'); 
else disp('Test  Pedigree pb 1: PB'); is_OK=false; end

fg = example_Pedigree_pb2();
rg = gm_rg_BETHE(fg);
[b, stop_by] = gm_infer_GBP( fg , rg, 100, 0.0001, 0.0, false);
%b_expected=b; stop_by_expected=stop_by; save('EXPECTED/GBP92.mat','b_expected','stop_by_expected');
load('EXPECTED/GBP92.mat')
if compareGBP(b,stop_by, b_expected,stop_by_expected); disp('Test Pedigree pb 2: OK'); 
else disp('Test  Pedigree pb 2: PB'); is_OK=false; end

