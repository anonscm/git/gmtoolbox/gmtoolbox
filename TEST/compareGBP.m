function is_OK = compareGBP (b, stop_by, b_expected, stop_by_expected)
is_OK = true;


for v=1:length(b)
    if any(abs(b{v}-b_expected{v})>0.0001); is_OK=false; end 
end

for isfg=1:length(stop_by)
    if ~strcmp(stop_by{isfg},stop_by_expected{isfg}); is_OK=false; end
end
end
