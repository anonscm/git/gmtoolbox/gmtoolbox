% Test gm_rg_JT.m

disp('***** Test gm_rg_JT.m *****')
clear fg rg rg_expected;

fg = example_Tiny();
rg = gm_rg_JT(fg); 
%rg_expected=rg; save('EXPECTED/rg_JT1.mat','rg_expected');
load('EXPECTED/rg_JT1.mat')
if compareRG(rg, rg_expected); disp('Test Tiny: OK'); 
else disp('Test Tiny: PB'); is_OK=false; end
clear fg rg rg_expected;


fg = example_Trivial();
rg = gm_rg_JT(fg); 
%rg_expected=rg; save('EXPECTED/rg_JT2.mat','rg_expected');
load('EXPECTED/rg_JT2.mat')
if compareRG(rg, rg_expected); disp('Test Trivial 1: OK'); 
else; disp('Test Trivial 1: PB'); is_OK=false; end
clear fg rg rg_expected;


fg = example_Lattice();
rg = gm_rg_JT(fg); 
%rg_expected=rg; save('EXPECTED/rg_JT3.mat','rg_expected');
load('EXPECTED/rg_JT3.mat')
if compareRG(rg, rg_expected); disp('Test Lattice 1: OK'); 
else; disp('Test Lattice 1: PB'); is_OK=false; end
clear fg rg rg_expected;


fg = example_YedidiaFig4();
rg = gm_rg_JT(fg); 
%rg_expected=rg; save('EXPECTED/rg_JT4.mat','rg_expected');
load('EXPECTED/rg_JT4.mat')
if compareRG(rg, rg_expected); disp('Test Figure4 1: OK'); 
else; disp('Test Figure4: PB'); is_OK=false; end
clear fg rg rg_expected;


global NS; NS=3; fg = gm_example_DBN(5,5);
rg = gm_rg_JT(fg); 
%rg_expected=rg; save('EXPECTED/rg_JT5.mat','rg_expected');
load('EXPECTED/rg_JT5.mat')
if compareRG(rg, rg_expected); disp('Test DBN 5-3-5 1: OK'); 
else; disp('Test DBN 5-3-5 1: PB'); is_OK=false; end
clear fg rg rg_expected NS;


fg=gm_example_Sprinkler;
rg=gm_rg_JT(fg,[1 3 2 4]);
%rg_expected=rg; save('EXPECTED/rg_JT6.mat','rg_expected');
load('EXPECTED/rg_JT6.mat')
if compareRG(rg, rg_expected); disp('Test Sprinkler 1: OK'); 
else; disp('Test Sprinkler 1: PB'); is_OK=false; end

FG.Card=2*ones(1,8);
FG.sfg{1}.Vcode=1:4;
FG.sfg{2}.Vcode=5:8;
FG.sfg{1}.F=fg.sfg{1}.F;
FG.sfg{2}.F=fg.sfg{1}.F;
FG.sfg{1}.BG=fg.sfg{1}.BG;
FG.sfg{2}.BG=fg.sfg{1}.BG;
rg=gm_rg_JT(FG,[1 5 7 2 6 3 8 4]);
%rg_expected=rg; save('EXPECTED/rg_JT7.mat','rg_expected');
load('EXPECTED/rg_JT7.mat')
if compareRG(rg, rg_expected); disp('Test Sprinkler 2: OK'); 
else; disp('Test Sprinkler 2: PB'); is_OK=false; end
clear fg rg rg_expected FG;


