% Test gm_include_evidence.m

disp('***** Test gm_include_evidence.m *****')
clear sfg ssfg_expected evidence;

fg = example_Tiny();
evidence = [0 1];
fg = gm_include_evidence(fg, evidence); sfg = fg.sfg{1};
sfg_expected.Vcode=sfg.Vcode;
sfg_expected.F{1}.V=1; 
sfg_expected.F{1}.T=[0.2; 0.1]; 
sfg_expected.BG=[true];
if compareSFG(sfg, sfg_expected); disp('Test Tiny: OK'); 
else disp('Test Tiny: PB'); is_OK=false; end
clear fg sfg_expected evidence;

fg = example_Trivial();
evidence = [2 0];
fg = gm_include_evidence(fg, evidence); sfg = fg.sfg{1};
sfg_expected.Vcode=2;
sfg_expected.F{1}.V=1; 
sfg_expected.F{1}.T=[0.6; 0.1];
sfg_expected.BG=[true];
if compareSFG(sfg, sfg_expected); disp('Test Trivial 1: OK'); 
else; disp('Test Trivial 1: PB'); is_OK=false; end
clear fg sfg sfg_expected evidence;

fg = example_Trivial(); % all variables observed
evidence = [1 2]; 
fg = gm_include_evidence(fg, evidence); 
if isempty(fg.sfg); disp('Test Trivial 2: OK'); 
else; disp('Test Trivial 2: PB'); is_OK=false; end
clear fg sfg_expected evidence;

fg = example_Trivial(); % none variable observed
evidence = [0 0];  
fg = gm_include_evidence(fg, evidence); sfg = fg.sfg{1};
sfg_expected.Vcode=[1 2];
sfg_expected.F{1}.V=1; sfg_expected.F{2}.V= [1 2]; 
sfg_expected.F{1}.T=[0.4; 0.6]; sfg_expected.F{2}.T= [0.2, 0.1; 0.6, 0.1]; 
sfg_expected.BG=[true false; true true];
if compareSFG(sfg, sfg_expected); disp('Test Trivial 3: OK'); 
else; disp('Test Trivial 3: PB'); is_OK=false; end
clear fg sfg  sfg_expected evidence;

fg = example_Lattice();
evidence = [0 0 4 0 0 0 0 0 1];
fg = gm_include_evidence(fg, evidence);
%fg_expected=fg; save('EXPECTED/evidence_Lattice1.mat','fg_expected');
load('EXPECTED/evidence_Lattice1.mat')
if compareFG(fg, fg_expected); disp('Test Lattice 1: OK'); 
else; disp('Test Lattice 1: PB'); is_OK=false; end
clear fg fg_expected evidence;

