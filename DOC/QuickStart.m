% Command lines of GMtoolbox QuickStart
format compact

% Quick start to learn how to use GMtoolbox (see http://www.inra.fr/mia/T/GMtoolbox).
% For a detailed presentation of formalization and inference in a Graphical Model (GM)
% with the toolbox, please refer to the user documentation of the toolbox (included in the toolbox).

clc

disp('You must be in gmtoolbox/DOC directory when launching QuickStart');
fprintf('\n ............ Pause (Type a key to continue)........................... \n');pause

GMtoolbox_path = pwd;
%addpath( genpath( GMtoolbox_path ) )
addpath( genpath( '../gmtoolbox' ) )

%% A toy DBN inference ecology problem ******************************
fprintf('\n\n ********************* Define a toy DBN inference ecology problem ********************* \n');

% We consider a very simplified ecological network, 4 species: plankton (P), zooplankton (Z),
% krill (K), marine mammals (M) which population dynamic are interdependent. 
% We define the list of variables as follows: $P^1,Z^1,K^1,M^1,P^2,Z^2,K^2,M^2,P^3,Z^3,K^3,M^3$.
fprintf('\n >> fg = gm_example_Ecology() \n ')
fg = gm_example_Ecology()
fprintf('\n >> gm_plot_fg( fg ) \n ')
gm_plot_fg( fg )
% Check the validity of the description with the  \courrier{gm\_check\_fg} function.
fprintf('\n >> [is_OK, msg] = gm_check_fg( fg ) \n ')
[is_OK, msg] = gm_check_fg( fg )

fprintf('\n ............ Pause (Type a key to continue)...........................');pause


% Inference of beliefs with different region graphs
fprintf('\n\n *** Infer beliefs with different region graphs *** \n');
fprintf('\n >> rg = gm_rg_JT( fg) \n ')
rg = gm_rg_JT( fg)
fprintf('\n >> [b, stop_by] = gm_infer_GBP ( fg , rg ) \n ')
[b, stop_by] = gm_infer_GBP ( fg , rg )
fprintf('\n >> b{9} \n ')
b{9}

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

fprintf('\n\n >> rg = gm_rg_BETHE( fg) \n ')
rg = gm_rg_BETHE( fg)
fprintf('\n >> [b, stop_by] = gm_infer_GBP ( fg , rg ) \n ')
[b, stop_by] = gm_infer_GBP ( fg , rg )
fprintf('\n >> b{9} \n ')
b{9}

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

fprintf('\n\n >> rg = gm_rg_CVM( fg) \n ')
rg = gm_rg_CVM( fg)
fprintf('\n >> [b, stop_by] = gm_infer_GBP ( fg , rg ) \n ')
[b, stop_by] = gm_infer_GBP ( fg , rg )
fprintf('\n >> b{9} \n ')
b{9}

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

% Take into account observations --------------------------------------
fprintf('\n\n *** Take into account observations *** \n');

fprintf('\n >> evidence=zeros(1,length(fg.Card)); \n ')
evidence=zeros(1,length(fg.Card));
fprintf('\n >> evidence([5 6 7 8])=1; \n ')
evidence([5 6 7 8])=1; % observation of states at the 2nd time step
fprintf('\n >> fge = gm_include_evidence(fg, evidence) \n ')
fge = gm_include_evidence(fg, evidence)

fprintf('\n\n >> gm_plot_fg( fge ) \n ')
gm_plot_fg( fge )

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

fprintf('\n\n *** Infer new beliefs *** \n ');
% Taking into account these observations, the initial sub factor graph is splitted in 5
% sub factor graphs.
fprintf('\n >> rge = gm_rg_JT( fge); \n ')
rge = gm_rg_JT( fge);
fprintf('\n >> [b, stop_by] = gm_infer_GBP ( fge , rge ) \n ')
[b, stop_by] = gm_infer_GBP ( fge , rge )
fprintf('\n >> b{9} \n ')
b{9}
% Now, the probability of the phytoplankton to be low (state 1) is higher (0.9615 instead of 0.2705).
% The others ways of building region graph here provide the same values for beliefs.

fprintf('\n ............ Pause (Type a key to continue)...........................');pause





clc
%% A toy Coupled HMM epidemics problem *******************************
% A Coupled HMM is used to model the dynamics of a pest that can spread on
% a landscape composed of $N$ crop fields organized in a regular grid.
% The neighborhood of field $i$, denoted $N_i$, is the set of the 4 closest
% fields (or 3 or 2, on the borders and corners of the grid).
% We consider a square grid of N=4 fields evolving during T=2 periods.
% We define the list of variables as follows : \\
% $H_1^1,H_2^1,H_3^1,H_4^1,H_1^2,H_2^2,H_3^2,H_4^2,H_1^3,H_2^3,H_3^3,H_4^3,O_1^2,O_2^2,O_3^2,O_4^2,O_1^3,O_2^3,O_3^3,O_4^3$ \\
% and we label them $1, \cdots ,20$. That is, state variables are coded  $1, \cdots ,12$ and
% observation variables are coded  $13, \cdots ,20$. 
fprintf('\n\n ********************* Define a toy Coupled HMM epidemics problem ********************* \n');

fprintf('\n >> N=4; \n');
N=4; % 4 fields
fprintf('\n >> T=2; \n');
T=2; % 2 time steps
Name = {'H_1¹','H2¹','H3¹','H4¹', ...
        'H1²','H2²','H3²','H4²', ...
        'H1³','H2³','H3³','H4³', ...
        'O1²','O2²','O3²','O4²', ...
        'O1³','O2³','O3³','O4³'};
fprintf('\n >> fg = gm_example_CHMM(N, T); \n');
fg = gm_example_CHMM(N, T);
fprintf('\n >> fg.Name = Name; \n');
fg.Name = Name;

% Each variable has 2 states: 1: infected, 2: non infected.
%fg.Card

% We suppose that the initial states of fields are unknown.  $P(H_i^1=1)=0.5$ with $i=\{1, 2, 3, 4\}$ 
% We then represent this \emph{a priori} knowledge on initial states with factors f9, f10, f11, f12.
% For example, considering factor f1, the set of associated variable is ${H_1^1}$ coded 1
% and the table is $P(H_i^1=1)=0.5, P(H_i^1=2)=0.5$.
%fg.sfg{1}.F{9}
%fg.sfg{1}.F{9}.T

% We now define factors involved in time transitions: $f1, \cdots ,f12$. Considering factor f1,
% the implied variables are $H_1^1, H_2^1,H_3^1, H_1^2$ which are labeled 1, 2, 3, 5.
% The associated table gives $P(H_1^2 | H_1^1, H_2^1,H_3^1)$
%fg.sfg{1}.F{1}.V
%fg.sfg{1}.F{5}.T

% Finally, we define factors involving observation variables.
% Considering factor f13, it involves 2 variables $O_1^2$ and $H_1^2$ coded 5 and 13.
% We know that $P(H_1^2=1 | O_1^2=2) = fn$ and \\ $P(H_1^2=2 | O_1^2=1) = fp$. This allows 
% to define the associated table.

%fg.sfg{1}.F{13}.V
%fg.sfg{1}.F{13}.T

fprintf('\n >> gm_plot_fg(fg) \n');
gm_plot_fg(fg)

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

fprintf('\n >> gm_plot_fg(fg, 1, ''circle'') \n');
gm_plot_fg(fg, 1, 'circle')

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

% subsection{Inference of beliefs -------------------------------
fprintf('\n\n *** Infer beliefs with different region graphs *** \n\n');
fprintf('\n >> rg = gm_rg_JT(fg); \n');
rg = gm_rg_JT(fg);
fprintf('\n >> [b, stop_by] = gm_infer_GBP ( fg , rg ); \n');
[b, stop_by] = gm_infer_GBP ( fg , rg );
fprintf('\n >> stop_by \n');
stop_by
fprintf('\n >> b{9}(1) \n');
b{9}(1)

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

fprintf('\n >> rg = gm_rg_BETHE(fg); \n');
rg = gm_rg_BETHE(fg);
fprintf('\n >> [b, stop_by] = gm_infer_GBP ( fg , rg ); \n');
[b, stop_by] = gm_infer_GBP ( fg , rg );
fprintf('\n >> stop_by \n');
stop_by
fprintf('\n >> b{9}(1) \n');
b{9}(1)

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

fprintf('\n >> rg = gm_rg_CVM( fg );  \n');
rg = gm_rg_CVM( fg ); 
fprintf('\n >> [b, stop_by] = gm_infer_GBP ( fg , rg ); \n');
[b, stop_by] = gm_infer_GBP ( fg , rg );
fprintf('\n >> stop_by \n');
stop_by
fprintf('\n >> b{9}(1) \n');
b{9}(1)

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

% subsection{Taking observations into account -----------------------
% If we only observe fields at time t=2, all were non infected : $O_1^2=2, O_2^2=2, O_3^2=2, O_4^2=2$.
% The considered variables are coded 13, 14, 15, 16.\\
% We just have to enter this evidence in the factor graph and infer probabilities on the new factor graph.
fprintf('\n\n *** Take into account observations *** \n');

fprintf('\n >> evidence = zeros(1, length(fg.Card)); \n');
evidence = zeros(1, length(fg.Card));
fprintf('\n >> evidence(17:20)=[1 2 2 2]; \n');
evidence(17:20)=[1 2 2 2];
fprintf('\n >> fge = gm_include_evidence(fg, evidence) \n');
fge = gm_include_evidence(fg, evidence)

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

fprintf('\n\n *** Infer new beliefs *** \n');
fprintf('\n >> rge = gm_rg_JT(fge); \n');
rge = gm_rg_JT(fge);
fprintf('\n >> [be, stop_by] = gm_infer_GBP ( fge , rge ); \n');
[be, stop_by] = gm_infer_GBP ( fge , rge );
fprintf('\n >> stop_by \n');
stop_by
fprintf('\n >> be{1}(1) \n');
be{1}(1)

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

fprintf('\n >> ge = gm_rg_BETHE(fge); \n');
rge = gm_rg_BETHE(fge);
fprintf('\n >> [be, stop_by] = gm_infer_GBP ( fge , rge ); \n');
[be, stop_by] = gm_infer_GBP ( fge , rge );
fprintf('\n >> stop_by \n');
stop_by
fprintf('\n >> be{1}(1) \n');
be{1}(1)

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

fprintf('\n >> rge = gm_rg_CVM(fge); \n');
rge = gm_rg_CVM(fge);
fprintf('\n >> [be, stop_by] = gm_infer_GBP ( fge , rge ); \n');
[be, stop_by] = gm_infer_GBP ( fge , rge );
fprintf('\n >> stop_by \n');
stop_by

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

fprintf('\n\n *** Plot beliefs evolution *** \n');
fprintf('(to reduce execution time, maximum iteration is set to 30 instead of 100)\n');
%bt = gm_plot_belief_evolution ( 1, 1, 1, 100, fge, rge, 0.0001, 0.0 );
% 100 may be long, 30 run is enough to see oscillations !
fprintf('\n >> bt = gm_plot_belief_evolution ( 1, 1, 1, 30, fge, rge, 0.0001, 0.0 ); \n');
bt = gm_plot_belief_evolution ( 1, 1, 1, 30, fge, rge, 0.0001, 0.0 );

fprintf('\n ............ Pause (Type a key to continue)...........................');pause

% Lets try to use damping, that is make a compromize at each iteration between old and new values
% of messages. There is still oscillation with damp at 0.1, so we try damp at 0.2.
fprintf('\n\n *** Infer new beliefs using damping *** \n\n');
fprintf('\n >> [be, stop_by] = gm_infer_GBP ( fge , rge, 100, 0.0001, 0.2, true); \n');
[be, stop_by] = gm_infer_GBP ( fge , rge, 100, 0.0001, 0.2, true);
fprintf('\n >> stop_by \n');
stop_by
fprintf('\n >> be{1}(1) \n');
be{1}(1)

fprintf('\n ........................... End of Quicktart ...........................\n');
